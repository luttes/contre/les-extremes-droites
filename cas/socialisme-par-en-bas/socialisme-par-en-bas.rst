
.. index::
   pair: SPEB  ; Rouges-bruns
   pair: SPEB ; islamogauchiste
   ! islamogauchiste
   ! SPEB (Socialisme par en bas)

.. _speb:

============================================================================
**Socialisme par en bas (SPEB)** antérieurement Socialisme International
============================================================================


**SPEB (Socialisme par en bas)** a été la branche française d'un réseau trotskyste
international constitué autour du SWP (Socialist Workers Party) britannique,
qui a théorisé (puis pratiqué) la possibilité d'alliances avec les courants
islamistes.

X pourrait en dire plus sur la centrale britannique.

L'expression "islamogauchiste", que l'extrême droite, LR et la macronie utilisent
comme stigmatisation large et indéfinie (notamment contre ceux qui luttent
légitimement contre l'islamophobie), a bien avec le SWP et SPEB une pertinence.

SPEB s'est appelé antérieurement Socialisme International (ils ont notamment
fait un peu d'entrisme chez les jeunes du PS).

A partir de la victoire du FIS islamiste aux élections locales algériennes
de 1990, leur ligne a été un **"soutien critique au FIS"**.

SPEB est rentré à la LCR en 2004 (Danièle Obono et le député marseillais LFI
Hendrik Davi ont adhéré à la LCR avec ce groupe), à cause de discussions
internationales entre la LCR et le SWP, et a donc aussi été partie-prenante
du NPA.
**Leurs pratiques étant assez manipulatrices**, ils ont par exemple essayé
de phagocyter le "réseau libertaire" que nous avions constitué au sein du NPA.

Avec les discussions entre LCR et NPA, un militant du SWP installé en France,
Sebastian Budgen, va jouer un certain rôle. Il a intégré aux des années 2000
le comité éditorial de la collection "La discorde" dirigée par Daniel Bensaïd
aux éditions Textuel et le comité de rédaction de **la revue ContreTemps que
j'ai créé en 2001 avec Bensaïd**.

Il aura des effets toutefois limités : Bensaïd ne publiera jamais de traductions
de livres de chefs du SWP dans sa collection, malgré les propositions réitérées
de Budgen et la gratuité  des traductions.

A l'époque, profondément transformé par la proximité de la mort (il avait le
Sida et a failli plusieurs fois y passer au cours de ses années), Bensaïd
préférait publier les livres issus de notre petit groupe non marxiste
(SELS, Sensibilité écologiste libertaire et radicalement sociale-démocrate,
créée fin 1997 avec des militants qui avaient pu passer par le PS, les Verts
et/ou les organisations anarchistes, tout en étant marqués par la sociologie
de Pierre Bourdieu, pour se rapprocher de la LCR).

J'ai revu Budgen récemment sur Internet : c'est lui qui faisait la traduction
de Judith Butler lors de la fameuse réunion publique de Pantin.

Je ne suis pas sûr que SPEB existe toujours.

Je ne sais pas ce que ses militants ont choisi entre:

- Obono-Davi à LFI,
- Révolution Permanente (scission du NPA issue du courant "moreniste" du
  trotskysme, né en Argentine, très peu présent en France jusqu'à un certain
  succès militant récent, et particulièrement en milieu étudiant, de RP),
- NPA Officiel
- et NPA Révolutionnaires ???

Peut-être que des entomologiste du mouvement trotskyste le savent ?
