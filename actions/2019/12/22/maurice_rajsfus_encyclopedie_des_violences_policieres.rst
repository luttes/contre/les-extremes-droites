.. index::
   pair: Maurice Rajsfus ; Article (2019_12_22)


.. _maurice_rajsfus_2019_12_22:

========================================================================
**Maurice Rajsfus, encyclopédie des violences policières**
========================================================================

.. seealso::

   - https://www.liberation.fr/france/2019/12/22/maurice-rajsfus-encyclopedie-des-violences-policieres_1770697
   - https://fr.wikipedia.org/wiki/Maurice_Rajsfus


.. contents::
   :depth: 3


Introduction
=============

Le journaliste, dont les parents arrêtés par un policier voisin sont morts en
déportation après la rafle du Vél d’Hiv, a documenté les dérives des forces
de l’ordre bien avant les gilets jaunes, de Mai 68 à 2014.

A 91 ans, il veut transmettre ses archives.


Historien de la répression

C’est peu de dire que l’homme entretient à l’égard des forces de l’ordre une
certaine animosité.
Elle l’amène par exemple à considérer que «la police de la République n’a jamais
été républicaine». De quoi faire bondir jusqu’au chef de l’Etat, qui récuse
depuis des mois l’expression même de «violences policières» : «Ils peuvent
récuser tout ce qu’ils veulent.
La violence policière, elle est dans l’ADN du policier. Quand il y a des
brutalités sans nom, on nous dit simplement qu’ils ont effectué des gestes
enseignés en école de police.»

Les titres de ses livres parlent d’eux-mêmes : la Police hors-la-loi,
les Mercenaires de la République ou encore Je n’aime pas la police de mon pays.

C’est peu de dire, aussi, que l’homme a ses raisons.

Le matin du 16 juillet 1942, le jeune Maurice - âgé de 14 ans - et sa famille
sont arrêtés chez eux par deux policiers. L’un d’eux, patronyme Mulot, est leur
voisin de palier.
C’est la rafle du Vél d’Hiv : 13 000 Juifs embarqués par les forces de l’ordre
françaises sur commande du régime nazi.

Moins d’une centaine survivra à la déportation.

Maurice Rajsfus et sa sœur devront leur vie à un ordre qui permettra la
libération des enfants français âgés de 14 à 16 ans. Et surtout à leur mère,
qui leur dira «partez de là», quand d’autres garderont leurs enfants auprès
d’eux.
Ses parents, Juifs polonais qui vendaient des chaussettes à Aubervilliers,
ne reviendront pas.
