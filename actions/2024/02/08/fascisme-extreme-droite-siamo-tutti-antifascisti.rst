.. index::
   pair: Luna ; Fascisme & Extrême-droite – siamo tutti antifascisti (2024-02-08)
   ! Fascisme & Extrême-droite – siamo tutti antifascisti (2024-02-08)

.. _antifa_2024_02_08:

================================================================================
2024-02-08 **Fascisme & Extrême-droite – siamo tutti antifascisti** par Luna
================================================================================

- https://lunatopia.fr/blog/fascisme-extreme-droite-siamo-tutti-antifascisti
- https://oisaur.com/@luna.rss

Annonce sur mastodon
========================

- https://oisaur.com/@luna/111897839274987492

Je viens de publier un article sur lequel je travaille depuis (bien trop)
longtemps : Fascisme & Extrême-droite – siamo tutti antifascisti

C'est un nouveau format, que je compte décliner sur plusieurs sujets : le
✨ vade-mecum ✨

J'y rassemble tout ce que j'ai vu, lu ou écouté d'instructif sur un sujet
précis, pour constituer une base de connaissance partageable et mise à jour
régulièrement.

Le retoot est apprécié 💚

Fascisme & Extrême-droite – siamo tutti antifascisti
======================================================

Depuis l’arrêt de mes articles « Dans mon historique », je réfléchi au
format qui me conviendrait pour les remplacer. Je l’ai dit dans le dernier
en date, je sature du format, et j’ai envie d’écrire plus, de donner mon
avis, au lieu de me contenter de relayer celui des autres.

Et pourtant, je n’ai presque rien publié depuis janvier 2022 : un article
sur mes débuts en linogravure, et trois articles sur mes lectures du début
et de la fin de l’année, ainsi que sur Mon territoire de Tess Sharpe, mon
roman préféré de 2022. [Edit : j’ai mis tellement de temps à finir cet
article que j’ai depuis repris la publication des Dans mon historique. Mais
le constat de mon ras le bol tient toujours.]

En revanche, après une grosse période de ras le bol généralisé, j’ai
recommencé à lire, à écouter, à me renseigner. Mais au lieu de me laisser
porter par l’actualité et surtout par les paniques morales du moment,
c’est-à-dire par l’agenda médiatique de la droite et l’extrême-droite ;
j’ai décidé de me tourner vers des lectures de fond, plus théoriques, ou en
tout cas qui conservent leur intérêt une fois passée la polémique du moment.

Ce qui m’a donné l’idée d’un nouveau type d’article, recueil de
liens là encore, mais avec plus de commentaires de ma part, et surtout :
thématique. L’idée étant de pouvoir y revenir et les compléter au fur
et à mesure que j’engrange des ressources et des références sur un sujet
précis, afin de me constituer une base de connaissance militante à laquelle
me référer et à partager, à la façon d’un vade-mecum en quelque sorte.

Et on commence par un sujet léger : le fascisme & l’extrême-droite.
