.. index::
   pair: Articles ; 2020-01-31

.. _centrisme_autoritaire_2020_01_31:

==================================================================================
**Centrisme autoritaire** : top 10 des "modérés", champions du *mépris de classe*
==================================================================================

.. seealso::

   - https://www.marianne.net/politique/centrisme-autoritaire-top-10-des-moderes-champions-du-mepris-de-classe
   - https://twitter.com/MarianneleMag/status/1223546655682826241?s=20

.. contents::
   :depth: 3


.. figure:: macron/extremistes.png
   :align: center

   https://twitter.com/MarianneleMag/status/1223546655682826241?s=20


Introduction
==============

Ils sont journalistes, commentateurs, anciens ministres ou députés : "Marianne"
a sélectionné les **compagnons de route les plus emblématiques de la dimension
autoritaire du macronisme**, sous ses allures modérées, bienveillantes et disruptives.

Qui aurait pu prédire, en mai 2017, que de sages centristes issus de la bonne
société **grimperaient si vite en agressivité** ?
A l'époque, ils étaient nombreux à vanter la modernité et la modération du
nouveau président Emmanuel Macron. Contre les extrêmes, pour réconcilier les
modérés de gauche et de droite, ils s'étaient retrouvés de concert aux urnes
et dans les médias de grande écoute afin de "faire barrage" à Marine Le Pen
au second tour de la présidentielle.

**Deux ans plus tard, l'évolution est saisissante**. Autrefois parangons de
bienveillance, certains traitent avec un confondant mépris le moindre opposant
au gouvernement.

D'autres, un temps attachés aux libertés publiques, justifient les reculs
démocratiques et la centralisation du pouvoir.

Ces personnalités établies - journalistes, intellectuels, essayistes - ont leur
rond de serviette dans les médias et se distinguent par leur soutien affiché,
ou très visible, à Emmanuel Macron.
Marianne en a sélectionné dix parmi les plus emblématiques.


.. _brice_couturier:

1er : Brice Couturier, le journaliste et militant
==================================================

A l'heure des discussions sans fin sur les frontières du journalisme et du
militantisme, Brice Couturier offre une instructive synthèse : ancien maoïste
et assistant parlementaire rocardien entre 1981 et 1986, il dispose aujourd'hui
de sa propre émission sur France Culture, Le Tour de monde des idées, mais ne
se prive pas de défendre l'action du gouvernement **avec une ferveur qui confine
au fanatisme**.

Auteur d'une **biographie hagiographique** du président (Macron, un président
philosophe), Couturier est allé jusqu'à animer des débats pour le compte de
LREM lors de la campagne des européennes en 2019.
Mais il s'est surtout distingué par ses prises de position enflammées lors de
la crise des gilets jaunes, en théorisant très vite l'appartenance du mouvement
à "l'extrême droite" et… son contrôle à distance par la Russie.
"Poutine est à la manœuvre. Une petite guerre civile en France ferait bien
ses affaires", tweetait-il le 1er décembre 2018, jugeant que tous ses
contradicteurs sortaient des "usines à trolls de [leur] ami Poutine".

::

    Poutine est à la manoeuvre. Une petite guerre civile en France ferait bien ses affaires.
    — Brice Couturier (@briceculturier) December 1, 2018

Quelques jours plus tard, relayant un article du Canard enchaîné qui évoque les
prétendues ambitions de prise du pouvoir de généraux à la retraite, le
journaliste de France Culture juge que "l'opération Gilets Jaunes est en train
de révéler son véritable objectif."
A mesure que la crise s'intensifie, **l'ancien socialiste** monte en gamme : il
échafaude ainsi une curieuse théorie selon laquelle le "Parti des médias",
très "à gauche", serait décidé à favoriser les gilets jaunes par tous les moyens,
quitte, selon lui, à "ressortir l'affaire Benalla" lorsque la cote du président
remonte…

Le 1er décembre 2018, après la mise à sac de l'Arc de triomphe, Couturier sort
de ses gonds et en **appelle à la répression d'Etat** : "Mais bon sang !
Foutez-les tous en taule ! Rétablissez l'ordre !"

Les gilets jaunes ? "Des factieux, des vandales, des fascistes qui croient que
le pouvoir se prend dans la rue et non dans les urnes."

**Il va même jusqu'à encourager à demi-mots Emmanuel Macron d'invoquer
l'article 16 de la Constitution, qui autorise le président à exercer une
dictature temporaire, pour résoudre la crise**.

::

    Mais bon sang ! Foutez-les tous en taule ! Rétablissez l'ordre !
    — Brice Couturier (@briceculturier) December 1, 2018

    Article 16 ?
    — Brice Couturier (@briceculturier) December 1, 2018

En avril 2019, le journaliste recommande à un gilet jaune devenu black block
et se disant "prêt à mourir" d'envisager "la solution du suicide".

En septembre, sur l'antenne de France Culture, il légitime les violences
policières : "Les populistes polarisent sciemment le débat politique, l'hystérisent
et déclenchent des violences de rue telles que celles qu'ont illustré hélas
les gilets jaunes ces derniers temps.

Face à des tentatives insurrectionnelles, (...) il est difficile de ne pas
faire usage de l'autorité et même d'une certaine forme de répression."

Qu'il est curieux de voir celui qui perd régulièrement son sang-froid sur
son compte Twitter fustiger "la polarisation (j’aime ou je déteste),
l’irrespect des formes et la violence verbale" sur les réseaux sociaux…

.. _jean_quatremer:

2e : Jean Quatremer, le roi du bingo
=======================================

Le journaliste de Libération Jean Quatremer avait tôt laissé planer des indices
quant à son désamour pour le concept de respect de la volonté populaire : en 2016,
aux côtés d'Emmanuel Macron et Daniel Cohn-Bendit à Sciences Po, il affirmait
tout en subtilité au sujet du Brexit : "Je fais partie des gens qui poussent à
fond pour que ça soit dur, ils ont voulu sortir, on va respecter leur vote
jusqu'au bout, ils vont le bouffer jusqu'au bout !".

Mais c'est lors de la révolte des gilets jaunes que Jean Quatremer démontre
sur son compte Twitter toute l’affection qu'il porte au peuple en général,
et aux classes populaires en particulier : "Ce mouvement de beaufs me sort
par les oreilles" cingle-t-il, fustigeant "un mouvement de beaufs d'extrême-droite".

Avant de réussir haut la main le bingo du centriste autoritaire : "beaufs poujadistes",
"France moisie", "factieux" qu’il est "vraiment temps d’embastiller",
"fascisme", "racistes", "homophobes"…

Carton plein, donc, pour Jean Quatremer, dont la furie contre les gilets jaunes
l’a conduit à se laisser tenter par les fake news, notamment en accusant les
gilets jaunes de constituer "un beau terrain d’action pour le Kremlin"
de Vladimir Poutine.
Mais peut-être fallait-il se rendre à l'évidence dès 2017, quand le journaliste
donnait le ton en affirmant sur Twitter : "Pour la première fois de ma vie,
je viens de faire un don à un parti politique, en l'occurrence En Marche…"

3e : Luc Ferry : l'héritier des anti-Communards
====================================================

Aimable philosophe libéral et ancien ministre de l'Education, Luc Ferry ne
semble a priori pas sujet aux poussées de fièvre. Mais en période de crise
sociale aiguë, la certitude de défendre la raison et la vérité face à une
foule devenue folle l'a fait dérailler.

De quoi rappeler, toutes proportions gardées bien sûr, les notables Versaillais
faisant tirer sur les insurgés pendant la Commune de Paris, en 1871.

Ainsi, en janvier 2019, au plus fort de la révolte des gilets jaunes, Luc Ferry
se lâche sur Radio Classique : "On est tous contre les violences, mais ce que
je ne comprends pas, c'est qu'on ne donne pas les moyens aux policiers de
mettre fin à ces violences", tonne l'ex-ministre de Jacques Chirac.

Quid des risques encourus alors par les manifestants ? "Et alors ?, rétorque-t-il.
Franchement, quand on voit des types qui tabassent à coups de pied un malheureux
policier qui est à terre, mais enfin…
Qu'ils se servent de leur arme une bonne fois.
Écoutez, ça suffit. Il y a un moment où ces espèces de nervis, ces espèces
de salopards d'extrême droite ou d'extrême gauche, ou des quartiers, qui
viennent taper du policier, ça suffit !"

Quelques secondes plus tard, Ferry en appelle à "la quatrième armée du monde,
capable de mettre fin à ces saloperies".

Plus tard, il précisera sur son compte Twitter n'avoir "jamais appelé à tirer
sur les gilets jaunes" mais demander à ce "que les policiers puissent se
servir, comme ils le demandent, de leurs armes non létales quand certains
cherchent carrément à les tuer."
Ce n'est donc pas avec des balles réelles mais à coups de LBD que Luc Ferry
souhaite voir opérer les forces de l'ordre.
Nous voilà rassurés !


4e : François Loncle, l'ancien député changé en troll
=======================================================

On s'est beaucoup arrêté sur la présence du patron de BlackRock France dans la
promotion de la Légion d'honneur de ce 1er janvier 2020 … mais François Loncle
y était aussi.
Parce qu'il n'est plus en fonction – retraité de ses activités de député – et
qu'il n'a plus rien à perdre, le septuagénaire porte en bandoulière le
**jusqu'au-boutisme des convertis au macronisme**.

Si l'homme a une longue carrière politique derrière lui au Parti socialiste,
il est désormais inactif, sauf sur son compte Twitter… dans lequel il tire
sur tout ce qui bouge.

Parmi les thématiques favorites de François Loncle : soutien effréné à
Emmanuel Macron, anathèmes contre les opposants, accusations en "populisme"
et en "fascisme", arguments complotistes contre les médias qui seraient
coalisés pour dire du mal du Président… mais aussi et surtout : rejet
viscéral des gilets jaunes, ceux qu’il appelle la "racaille jaune".

::

    La racaille jaune sur les Champs est très peu nombreuse même si elle fait du bruit .
    — Francois Loncle (@LoncleFrancois) July 14, 2019

Jérôme Rodrigues invité de C politique ? Une "irresponsabilité" pour un tel
"abruti".

Les médias ? Ceux-ci font tous dans le "Macron bashing".

Il demeure que François Loncle a obtenu "la récompense de mérites éminents
acquis au service de la nation". Après avoir été journaliste à l'ORTF où il
participera à Mai 68, Loncle se rapproche de la CFDT en 1969, un syndicat dont
il nous confie se sentir "toujours très proche".

Puis c'est le PS, auquel il restera fidèle jusqu'en 2017.

De Mai 68 et du Programme commun de 1981 à la défense des réformes libérales
d’Emmanuel Macron en 2020 ? François Loncle se défend de tout manque de
cohérence : "Du mitterandisme au macronisme, je n’ai pas changé, j’ai toujours
gardé une ligne que j’ai essayé de maintenir : je suis social-démocrate,
réformateur et progressiste, c’est pour cela que je soutiens Macron, qui est
le contraire d’un conservateur."
Il poursuit : "Je me définis avant tout comme réformiste, plutôt que comme
socialiste, un mot que je trouve trop vague."

Voilà comment François Loncle, ancien député PS, se revendique d’une forme de
cohérence à avoir fait Mai 68 tout en expliquant désormais qu’il "est effarant
que ces abrutis de GJ puissent continuer leurs méfaits", eux qui "ne méritent
désormais que le mépris".
Il persiste et signe auprès de Marianne : "Les gilets jaunes sont néfastes,
voilà le seul pays au monde où cela se passe comme ça. On donne une très
mauvaise image de la France !".

::

    À proximité du Champ de Mars, des petits groupes de gilets jaunes cassent
    du mobilier urbain et brûlent ce qu’ils trouvent . Des Parisiens et des
    touristes s’étonnent de la passivité de la police .
    Effarant que ces abrutis de GJ puissent continuer leurs méfaits( 13e samedi ! )
    — Francois Loncle (@LoncleFrancois) February 9, 2019


Une chose est sûre, François Loncle parle cru, il l’assume : "La langue de bois,
le politiquement correct, ce n’est pas pour moi ! Je préfère dire ce que je
pense."
Quant à ses tweets, l’ancien ministre se confesse : "Je reconnais que je
devrais moins tweeter, je vais me modérer davantage. Parfois je suis
excessif…"
Si peu !

5e : Xavier Gorce : le caricaturiste caricatural
====================================================

Vous ne connaissez peut-être pas son nom, mais vous avez sans doute croisé son
trait : Xavier Gorce et ses "indégivrables", dessins de manchots traitant avec
humour de l'actualité, se retrouvent régulièrement dans les pages du Monde.

Homme "de gauche" revendiqué lorgnant sévèrement vers le libéralisme,
l'illustrateur a très vite exhalé dans ses dessins sa détestation des gilets
jaunes (un mouvement qu'il juge "facho-plouc de droite anti-démocratique"),
n'hésitant pas à les injurier ouvertement en les assimilant qui à des
imbéciles, qui à des fascistes.
En la matière, les images parlent plus que les mots.


6e : Eric Le Boucher, l'éditorialiste en croisade contre "l’inculture des Français"
========================================================================================

Dans la grande famille des éditorialistes, il y a notamment Eric Le Boucher.
Le journaliste, co-fondateur de Slate et ancien chroniqueur au Monde, tient
désormais une chronique aux Echos et à l'Opinion.

Dans cette dernière, baptisée sobrement "Think again", il délivre un **concentré
chimiquement pur de centrisme autoritaire**.
Ainsi du 9 décembre 2018, lorsque Le Boucher se demande, "en cette ère de grand
infantilisme (…), comment éduquer le peuple et le 'corriger de ses idées folles
et inconsidérées' ?".

S'il pense que "le conflit des gilets jaunes a ses raisons", inutile de lui
donner trop d'importance : "Emmanuel Macron a une réponse d’ensemble, il s’est
présenté pour cela, il la met en œuvre : tirer la France vers le haut par sa
compétitivité et par une amélioration des compétences."

Au cœur de la crise : l’incapacité du peuple à saisir le sens des politiques
gouvernementales. Le Boucher nous mettait en garde dès novembre 2017 dans une
chronique des Echos intitulée "Macron face à l'inculture économique des Français !" :
"Voilà pourquoi les Français ne comprennent rien aux réformes macroniennes et
les 'politisent' de façon caricaturale.
Il faudra que le président de la République s'en préoccupe."

Deux ans plus tard, rien n'a changé : "Encore une fois, la colère de la classe
moyenne est légitime mais ce qui ne l’est pas, ce qui est infantile, c’est
l’ignorance inouïe des mécanismes simples de l’économie et de la démocratie.
(…) Tant que les gilets jaunes n’auront pas pris en compte ces contraintes,
les discussions avec eux seront, des deux côtés, frustrantes."

La réforme des retraites ? Le 12 janvier 2020 dernier Eric Le Boucher écrivait
que "C’est un sujet froid qui devrait s’élaborer sur les chiffres, mais
touchant le travail et la vie après le travail, il allume les passions.

Eau froide dans huile bouillante : c’est le geste culinaire que le gouvernement
aurait dû éviter, l’explosion en gouttes brûlantes était garantie. (…)

La réforme a plongé la France dans l’'hystérie collective."

Eric Le Boucher, adepte de la métaphore culinaire, n’a lui pas mis d’eau
dans son vin.
Il se fait même psychiatre du peuple, analysant avec finesse le fait que les
Français soutiennent à la fois la mobilisation et l'idée d'une réforme
universelle : "Allez comprendre… Autrefois, pour dire bipolaires, pour dire
schizophrènes, on disait fous, on disait bêtes".


7e : Romain Goupil : le soixante-huitard macronisé
======================================================

On ne sait pas trop par quel miracle, mais Romain Goupil a acquis le statut de
sommité des plateaux de télévision : LCI, France 2, le réalisateur est
partout pour donner son avis.

Son pedigree ? Ancien animateur de mai 1968 et Caméra d'or à Cannes en 1982.

Libéral-libertaire revendiqué et néo-conservateur convaincu - il a soutenu
toutes les interventions militaires depuis Sarajevo jusqu'à la guerre d'Irak -,
Goupil n'a hérité de son passé révolutionnaire que le tutoiement intempestif,
et soutient sans réserve Emmanuel Macron.

Il serait trop long de lister toutes les outrances du soixante-huitard, mais
à propos des gilets jaunes, Romain Goupil s'est surpassé, en tentant avec une
constance qui force le respect de les faire passer pour une bande de fascistes
imbéciles destinés à renverser la République.

Le fond du mouvement ? "C'est que des insultes, que du ressentiment, que de
la médiocrité, que de la bêtise", le tout poussé par "une collusion
jaune-rouge-brune" qui "menace la démocratie".

Les porte-paroles ? "Des imbéciles qui poussent à l'affrontement".
L'implication de Poutine, figure imposée du centrisme autoritaire, est également
de la partie.

8e : Bernard Henri-Lévy, chemise immaculée et propos orduriers
==================================================================

Nul n'ignore ses plus belles saillies, mais il méritait tout de même sa place
dans ce classement : Bernard Henri-Lévy.

Il affichait dès le 17 novembre 2018 la couleur : "Poujadisme des gilets jaunes.
Échec d’un mouvement qu’on nous annonçait massif. Irresponsabilité des chaînes
d’info qui attisent et dramatisent.
Soutien à Macron, à son combat contre les populismes et à la fiscalité écolo."

Puis vint le temps de l'amalgame : "La violence contre les juifs, les slogans
antisémites sont comme le terme de ce mouvement, l’aboutissement de cette
distillation.
On commence par le référendum d’initiative citoyenne et on finit par l’antisémitisme.
On commence avec Rousseau et on finit avec Doriot".

Cette sortie trouve alors son origine dans les insultes à caractère antisémite
proférées à l'encontre d'Alain Finkielkraut, en marge de "l’acte XIV" des gilets
jaunes, samedi 16 février 2020.
Pris à partie avec violence dans les rues de Paris, le philosophe avait notamment
été traité de "sioniste de merde", de "raciste".
Un évènement que Bernard Henri-Lévy n'avait pas tardé à identifier comme le
symptôme d'un mal plus général, s'étendant à tous les gilets jaunes.

"Ce ne sont pas les marges ça, c’est le cœur du mouvement !", avait-il assuré tout de go.

La panoplie ne serait pas complète sans l'inévitable intox destinée à discréditer
un peu plus les gilets jaunes.
BHL les qualifie ainsi en direct à la télévision de… chasseurs de Roms !
"On n’a pas raison de casser des policiers, de frapper des journalistes, de
faire des quenelles, de chasser les Roms et de casser des institutions de la
République", s’indignait ainsi sur CNews le lecteur assidu de Jean-Baptiste Botul.


9e : Yves Calvi : l’animateur pondéré
==========================================

Censé incarner la neutralité et la pondération, l’animateur vedette passé par
C dans l’air s’est laissé dépasser par la colère dans son émission
L'info du vrai le 8 janvier 2019.
Yves Calvi y clame son incompréhension des gilets jaunes. "On voit un soutien
relatif des Français à ce mouvement qui reste à mes yeux la chose la plus
mystérieuse et probablement la plus inquiétante".

Mystère que le soutien des Français à un tel mouvement, qu'il qualifie volontiers
d'escroquerie le 18 janvier 2019: "Le phénomène des gilets jaunes, au regard
de ce que l'on sait aujourd'hui, via Internet et la mobilisation sur les
ronds-points, n'est-il pas une escroquerie ? (…)
Une escroquerie intellectuelle… On s'est laissés impressionner par 287.000 personnes !".

Toujours dans l’Info du vrai, l’animateur fait montre de sa capacité à disrupter
la notion de maintien de l’ordre en suggérant des solutions peu usitées en
démocratie : "Si, en effet, les périmètres sont franchis, on doit procéder à
des arrestations.
Et à un moment, l’acte de police, et des policiers, il consiste à mettre des
gens… La seule solution… **c’est dans un stade**, avec tout ce que cela provoque
sur la plan imaginaire et historique.
Après, on pourrait dire qu’aujourd’hui, ils sont prévenus.
Innovant, en France en tout cas.

Évoquant le problème de la défiance envers les journalistes, Yves Calvi
s'insurgera : "Les journalistes sont décrits comme méprisant une partie des
Français ? C'est pas le cas du tout !"
Qui oserait en effet accuser celui qui indique que "la majorité de ceux qui
étaient sensibles aux idées des gilets jaunes étaient les moins diplômés
dans notre pays" d’être condescendant ?


10e : Jean-Louis Gagnaire, le prototype
==========================================

Peu renommé, Jean-Louis Gagnaire incarne à sa manière l'idéal-type du
**néolibéral autoritaire**. Sur son compte Twitter, cet ancien député PS tendance
deuxième gauche oscille entre propos agressifs, mépris de classe décomplexé
et soutien sans réserve au chef de l'Etat.

Ces temps-ci, Jean-Louis Gagnaire fustige les professions en grève.

Les profs qui détruisent des manuels voués au pilon ? "Cette minorité d’enseignants
ne méritent plus le respect de leurs (sic) élèves".

Les avocats qui jettent leur robe ? "On aurait pu imaginer qu'ils ne se
comportent pas comme des agents de catégorie C de la fonction publique".

Les pompiers qui manifestent ? "Chacun sait que ce sont des minorités de pompiers
radicalisés à l'extrême-droite".

Chez l'ex-socialiste, **la conversion au centrisme autoritaire** a peut-être
précédé Macron : après les attentats du 13 novembre 2015, il avait co-signé
une loi visant à rétablir la censure sur la presse, la radio, le cinéma
et le théâtre.

