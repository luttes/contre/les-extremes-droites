.. index::
   ! psychopathes


.. _acticles_2022_10:

==================================================
2022-10
==================================================


2022-10 Y a-t-il un psychopathe en nous ?42 - La réponse à presque tout
=============================================================================

- https://www.arte.tv/fr/videos/104841-007-A/y-a-t-il-un-psychopathe-en-nous/
- https://invidious.fdn.fr/watch?v=KplT_g4Cngk


- Réalisation Luise Donschen
- Pays Allemagne
- Année 2022

Si la figure du tueur en série fascine Hollywood, la plupart des psychopathes
ne sont pas des assassins.

Malins, ils se fondent dans la masse. Alors comment les reconnaître ?
Et sont-ils dangereux ?


"42 - La réponse à presque tout" est la série documentaire scientifique d'ARTE.

Clin d’œil au Guide du voyageur galactique de Douglas Adams dans lequel
le nombre 42 apporte la réponse ultime, elle décortique des questions
existentielles, et a réponse à tout... ou presque.

Série documentaire scientifique (Allemagne, 2022, 25mn)
disponible jusqu'au 16/10/2023

Voici les sources de cette émission :

1. Le cerveau des psychopathes, une étude réalisée par Timm Pöppl,
   équipe de recherche sur les fonctions cérébrales des psychopathes, Hôpital Universitaire d’Aix-la-Chapelle

2. Étude réalisée à Harvard sur la perturbation du circuit de la récompense
   chez les psychopathes www.sciencedaily.com/

3. Le psychopathe qui réussit, Psychopathie et vie professionnelle
    www.forschung-und-lehre.de/ka...

4. James Fallon – le neuroscientifique dont le cerveau avait les caractéristiques d’un cerveau de psychopathe
      www.faz.net/aktuell/gesellsch...

5. La psychopathie au quotidien –
   L’association pour l'éducation à la gestion de la psychopathie dans le milieu familial et dans la vie quotidienne, par notre experte Abigail Marsh
   psychopathyis.org/ #psycho #psychopath #psychopathie

#Psychopathe #Danger #ARTE

