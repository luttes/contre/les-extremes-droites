.. index::
   pair: FLF Voiron ; 16e Rencontres Luttes et Résistances RLF (2025-01-11)

.. _rlf_voiron_2025_01_11:

===================================================================================================================================================================================================================================================================
2025-01-11 **16e Rencontres Luttes et Résistances RLF: Le RN vous ment ! Il surfe sur les vagues de crises. Comment le faire tomber de sa planche ?** samedi 11 janvier 2025 à Voiron  
===================================================================================================================================================================================================================================================================

- https://visa-isa.org/article/rlf-voiron-16e-rencontres-luttes-et-resistances |visa|

.. figure:: images/affiche.webp
   :width: 400


Contact: rlfvoiron@rlf38.org
================================

- rlfvoiron@rlf38.org
- https://rlf38.org/ (pas mis à jour depuis janvier 2023)


Lieu: Salle des fêtes Place Jacques Antoine Gau 38500 Voiron |rlf_grenoble| 
==================================================================================

.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=5.586585402488709%2C45.367917906920624%2C5.590125918388367%2C45.36958179910426&amp;layer=mapnik" style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/45.368750/5.588356">Afficher une carte plus grande</a></small>

Description : Le RN vous ment ! Il surfe sur les vagues de crises. Comment le faire tomber de sa planche ?
===============================================================================================================

Voici le programme définitif des 16ème rencontres RLF (Ras l'Front) des
Luttes et Résistances à Voiron samedi 11 janvier 2025

Nous avons rencontré quelques défections parmi les intervenant-es et
avons dû nous adapter pour permettre à ces rencontres d'exister, **plus
que jamais cette année**.

Les stands associatifs seront ouverts tout l'après-midi, offrant une
belle opportunité pour échanger et découvrir des initiatives militantes
et locales.

14h00 - Ateliers d'éducation populaire
=============================================

Débat mouvant 
-----------------

Une discussion participative et dynamique pour explorer les idées et positions 
du public autour des questions sociales et politiques.

Arpentage : L'arpentage, c'est quoi ?
----------------------------------------------

Chaque participant-e lit une courte partie du livre qui lui est attribuée. 

Puis, chacun partage ce qu'il a compris de son passage, afin de créer un échange. 
L'objectif est d'intégrer collectivement le contenu du livre de façon active 
et critique et d'amener le débat.

Cet atelier proposera une réflexion sur deux ouvrages:

- :ref:`ugo_palheta_2025_01_11`
- :ref:`felicien_faury_2025_01_11`


.. _ugo_palheta_2025_01_11:

2025-01-11 **"Extrême droite, la résistible ascension"** sous la direction d'Ugo Palheta
==============================================================================================

- :ref:`resistible_ascension_2024`

Avec les contributions de:

- Cassandre Begous, 
- Marlène Benquet,
- Vincent Berthelier, 
- Samuel Bouron, 
- Charlène Calderaro, 
- Zoé Carle, 
- Johann Chapoutot, 
- Aurélie Dianara, 
- Didier Fassin, 
- :ref:`Félicien Faury <antisem:felicien_faury>` 
- Fanny Gallot,
- Clémence Guetté, 
- Yann Le Lann, 
- Mathieu Molard, 
- Stefano Palombarini,
- Pauline Perrenot, 
- Salvatore Prinzi. 

Les auteurs ne seront pas présents, mais leurs analyses nourriront nos discussions.


.. _felicien_faury_2025_01_11:

2025-01-11 **Des électeurs ordinaires, Enquête sur la normalisation de l’extrême droite** par Félicien Faury |FelicienFaury| àVoiron  (Isère)
==================================================================================================================================================

- :ref:`antisem:felicien_faury`
- :ref:`antisem:electeurs_ordinaires_2024_05`

"Des électeurs ordinaires Enquête sur la normalisation de l’extrême droite" 
de :ref:`Félicien Faury <antisem:felicien_faury>`,  sociologue, qui étudie 
les comportements politiques et les mécanismes sociaux influençant les choix 
électoraux. 

À travers cet ouvrage, il s'intéresse particulièrement aux électeurs du 
Rassemblement National, cherchant à comprendre les motivations et trajectoires 
sociales des électeurs de l'extrême droite, ainsi que l'impact de leur 
environnement sur leurs opinions politiques.

Ses recherches mettent en lumière les facteurs sociaux, économiques et
culturels qui expliquent l'adhésion à l'extrême droite dans certains
milieux populaires.

Webinaire de Félicien Faury le 7 novembre 2024
------------------------------------------------------

- :ref:`antiracisme_2024:felicien_faury_2024_11_07`


Autres liens
----------------

- :ref:`antisem:droitisation_2024_09_04`
- :ref:`antisem:pop_fascisme_2024`
- :ref:`antisem:le_temps_qui_reste_2023`
- :ref:`antisem:coulee_brune_2024`
- :ref:`idees_fausses_2023`
- :ref:`sentence_de_mort_2022`


15h30 - Dialogue avec Raphaël Arnault : Penser l'organisation sur le terrain pour ne pas laisser la place à l'extrême droite
==================================================================================================================================

Avec Raphaël Arnault, de la Jeune Garde lyonnaise et député LFI

Thème : Penser l'organisation sur le terrain pour ne pas laisser la place 
à l'extrême droite.

Raphaël partagera son analyse sur la lutte contre l'extrême droite,
l'importance de l'engagement citoyen et de l'organisation politique sur
le terrain.

18h00 - Place à la convivialité
======================================

Repas prix libre : Un moment de partage et de convivialité pour
continuer à échanger autour des thématiques de la journée et des
perspectives à venir.


20h00 - Spectacle : "Voix Vives et Colère sourde" De la compagnie et collectif de l'Âtre de La Tour du Pin, 
==================================================================================================================

Depuis des années, le Collectif de l'Âtre travaille autour des questions
sociologiques au sein des communes, recueillant les témoignages des
populations locales sur leurs origines et leurs relations avec le
territoire. À travers des témoignages et des extraits de textes
d?auteurs·trices, ce spectacle cherche à saisir l?impact de la présence
de l?extrême droite sur ces récits locaux.

Annonce sur mastodon
=======================

- https://kolektiva.social/@noamsw/113758501208422047


Autres rencontres de Voiron
=================================

- :ref:`rencontres_voiron`
