.. index::
   pair: Témoignages; Saint-Soline

======================================================================================
2023-03-27 **Témoignage de 3 jeunes concernant la répression à Sainte-Soline**
======================================================================================


Témoignage de Sébastien
===========================

- https://librinfo74.fr/temoignage-de-sebastien-de-la-confederation-paysanne-present-a-sainte-soline/

Lors du rassemblement organisé le jeudi 27 mars à Annecy pour dénoncer
les violences policières à Sainte-Soline, trois jeunes participant-es
ont témoigné de leurs expériences atroces vécues au cours de la manifestation
du samedi 22 mars.

Vous trouverez ci-dessous le témoignage de Sébastien Rocher de la confédération
paysanne dans un premeir temps.

Suivrons les témoignages de Ombeline et de Ingrid dans un second temps

Sébastien Rocher, de la confédération retrace ses trois jours à Sainte-Soline : (temps de lecture : 6 mn 49 s

Témoignage d'Ombeline
=======================


- https://librinfo74.fr/temoignage-de-ombeline-presente-a-sainte-soline/

Lors du rassemblement organisé le jeudi 27 mars à Annecy pour dénoncer
les violences policières à Sainte-Soline, trois jeunes participant-es
ont témoigné de leurs expériences atroces vécues au cours de la manifestation
du samedi 22 mars.

Vous trouverez ci-dessous le témoignage de Ombeline qui lit une lettre
de rupture avec l’État avec une sincérité poignante. (lecture 1 mn 31 s)

Témoignage d'Ingrid
=======================

-https://librinfo74.fr/temoignage-dingrid-presente-a-sainte-soline/

Lors du rassemblement organisé le jeudi 27 mars 2023 à Annecy pour dénoncer
les violences policières à Sainte-Soline, trois jeunes participant-es
ont témoigné de leurs expériences atroces vécues au cours de la manifestation
du samedi 22 mars 2023.

Vous trouverez ci-dessous le témoignage d’Ingrid dont la sincérité et la
force de ses paroles ont bouleversé et ému l’assistance. (temps de lecture 5mn 36s)



