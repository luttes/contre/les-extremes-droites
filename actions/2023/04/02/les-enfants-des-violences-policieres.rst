.. index::
   pair: Violences Policières; 2023-04-02


.. _violences_policieres_2023_04_02:

========================================================================
2023-04-02 Les enfants des violences policières
========================================================================

- https://blogs.mediapart.fr/arie-alimi/blog/020423/les-enfants-des-violences-policieres

Envers et contre l’évidence des témoignages, des corps et des vies brisés,
des avenirs interrompus des mutilés, de la douleur partagée, le gouvernement
persiste à taire et à faire taire l'expression de violences policières.

**Une violence sémantique qui va jusqu’à étouffer les mots qui la décrivent,
quitte à poursuivre devant la justice tous ceux qui la dénoncent**.


S’il devait y avoir une **règle éthique fondamentale** attachée à l’exercice
du pouvoir, ce serait celle de la transparence, celle de donner à voir
l’exercice et le résultat de son mandat, car le seul pouvoir, résulte
de la confiance donnée pour représenter.

En lieu et place de cette transparence, il y a quelque chose d’orange
mécanique, dans ce geste de l’Etat qui à la fois nous étrangle et nous
contraint à regarder la violence qui s’exerce sur nos propres corps.
