
.. _acticles_2023_12:

==================================================
2023-12
==================================================

.. _turchi_2023_12_01:

2023-12-01 **`Ultradroite`, extrême droite : deux appellations, une même idéologie** par Marine Turchi
===================================================================================================================

- https://www.mediapart.fr/journal/france/011223/ultradroite-extreme-droite-deux-appellations-une-meme-ideologie
- https://www.mediapart.fr/biographie/marine-turchi

Introduction
----------------

:term:`Ultradroite`: ces dernières années, le terme a fleuri
dans les médias, qui l’utilisent à tout-va, comme un synonyme 
d’"extrême droite hors Rassemblement national (RN)".

Dans la presse, il désigne pêle-mêle des groupes terroristes projetant
des attentats contre des élu·es ou des mosquées, des groupuscules menant
des attaques violentes contre des militant·es antifascistes ou des personnes
racisées, ou des formations idéologiquement radicales mais non violentes.

Devenue "fourre-tout", cette classification se heurte à la réalité
du terrain.  

Un terme issu de la sphère policière
-------------------------------------------

À l’origine, ce terme est issu de la sphère policière. En 1994, une affaire
d’"espionnage"d’un congrès à huis clos du Parti socialiste (PS)
par un agent des renseignements généraux (RG) provoque une réforme de ce
service : le suivi de l’activité des partis politiques est alors interdit,
mais pas celui des individus susceptibles, "au nom d’idéologies extrêmes
», de recourir à la violence physique ou de préparer des actions illégales,
et donc de constituer une menace pour l’ordre public.

Pour séparer clairement l’extrême droite violente de l’extrême droite
partisane (légaliste et électoraliste), les services de renseignement parlent
d' :term:`Ultradroite`, "évoquant les “ultras” de la guerre d’Algérie, 
explique à Mediapart l’historien Nicolas Lebourg, spécialiste des extrêmes droites.

Un groupe ultra partage ou propage des idées extrêmes et recourt à la
violence pour tenter de les imposer, de les défendre, de les faire avancer",
a précisé le patron de la Direction générale de la sécurité intérieure
(DGSI), Nicolas Lerner, lors de son audition par la commission d’enquête
sur la lutte contre les groupuscules d’extrême droite, en 2019. 

Outre le recours à la violence, ces groupes dits d’:term:`Ultradroite` partagent un
point commun : l’extraparlementarisme, c’est-à-dire un mode d’action
qui ne passe pas par des candidatures à des élections.

Le qualificatif, qui est aujourd’hui utilisé par la justice antiterroriste,
s’est imposé dans les médias à l’occasion des attentats commis par
Mohammed Merah en mars 2012, rappelle Nicolas Lebourg : "Les services de
police hésitaient alors entre attribuer ses crimes à la mouvance djihadiste
ou au milieu néofasciste, la presse a repris la formulation “ultradroite”,
qu’elle tenait des enquêteurs."Ce syntagme s’est ensuite développé
avec le retour au pouvoir de la gauche et les manifestations contre le mariage
pour tous émaillées de violences de groupuscules d’extrême droite.

Si cette formulation a un intérêt évident pour les fonctionnaires de police,
elle demeure plus délicate d’usage pour les sciences sociales et les médias,
souligne Nicolas Lebourg.

Des violences recensées aussi dans les formations partisanes
-------------------------------------------------------------------

Car ce distinguo présente un premier problème : il laisse penser que
l’extrême droite partisane serait forcément non violente et que les
groupuscules à l’idéologie radicale passeraient forcément à l’action
violente.

Or, dans l’ouvrage collectif Violences politiques en France, de 1986 à nos
jours (Presses de Sciences Po, 2021), qui présente des chiffres inédits (lire
notre Boîte noire), Nicolas Lebourg et la sociologue Isabelle Sommier comptent
douze formations d’extrême droite légalistes (partisanes ou syndicales)
impliquées dans des violences entre 1986 et 2016.

Parmi celles-ci, le parti de Marine Le Pen : "Sur 1 305 faits de violence
d’extrême droite recensés sur cette période, 89 proviennent de militants
du Front national", explique l’historien, qui souligne cependant que
l’arrivée de Marine Le Pen à la tête du parti a constitué "un vrai
cap", puisque entre 2011 et 2016, "il n’y a “que” quinze cas de
violences recensés".

"Si vous êtes un parti qui respecte les institutions, national-populiste,
mais que vous cognez dans un collage, vous êtes d’ultradroite ; si vous êtes
radical et non violent, vous n’êtes pas d’ultradroite", résume Nicolas
Lebourg. Il cite l’exemple de l’Institut Iliade, héritier de la Nouvelle
Droite, qui est "radical idéologiquement, mais pas d’ultradroite".

Même problème au niveau des idées, souligne La Croix. Si l’on retient,
pour définir l’ultradroite, le critère de l’illégalité – et donc les
expressions d’opinion délictuelles telles que la négation ou l’apologie
de crime contre l’humanité, l’incitation à la discrimination, à la
haine ou à la violence à raison de l’origine, de la couleur de peau ou de
la religion –, dans ce cas, Jean-Marie Le Pen (qui a été condamné pour
les deux) et Éric Zemmour (pour la seconde) pourraient être qualifiés de
représentants d’:term:`Ultradroite`.  

**La porosité entre "extrême droite» et `Ultradroite`**
------------------------------------------------------------

Tracer une frontière étanche entre les extrêmes droites partisane et
groupusculaire est impossible. Dans la pratique, les liens interpersonnels
et amicaux sont importants. Et il arrive que certains membres de formations
légalistes se rendent à des événements d’organisations violentes, ou
se joignent à celles-ci pour aller faire le coup de poing. Cette forme de
"double appartenance"est tolérée tant qu’elle reste discrète. Mais
lorsque des membres sont épinglés publiquement, des sanctions sont prises.

Cette porosité parcourt l’histoire du Rassemblement national. Le parti est
né, en 1972, de l’union de différentes chapelles – militants d’Ordre
nouveau (groupuscule néofasciste réputé pour sa violence), anciens de
l’OAS et ex-collaborationnistes.

Dans les années 1990, lorsque le Front national souhaite s’implanter davantage
dans les facultés et lycées, les militants du GUD et ceux du Front national
de la jeunesse (FNJ) se rapprochent et mènent des actions communes. L’une
d’elles vaudra d’ailleurs à des membres des deux formations de se retrouver,
en 1994, sur les bancs du tribunal de Nanterre, défendus par deux avocats :
Marine Le Pen et l’ancien chef du GUD Philippe Péninque.

À son arrivée à la tête du parti en 2011, Marine Le Pen fait un grand
ménage. Elle exclut un grand nombre de radicaux (historiques du Front national,
pétainistes de l’Œuvre française, etc.) et répète qu’elle n’a "
aucun rapport avec ces groupes", qui expriment d’ailleurs régulièrement
"leur désapprobation à [son] égard".

Malgré tout, des liens ont perduré, comme Mediapart l’a démontré dans
une grande enquête publiée en 2013. Y compris dans le premier cercle de la
présidente du Front national, qui a confié des postes clés à des anciens
du GUD accusés d’antisémitisme et encore présents, en mai 2023, dans le
défilé annuel des néofascistes, comme nous l’avons révélé (lire nos
enquêtes, ici et là).

Cette porosité s’est illustrée à travers les prestataires sécurité ou
communication du parti (lire nos enquêtes ici, là et là), mais aussi lors
de soirées mêlant gudards et dirigeants du FNJ, comme celle des 40 ans du
Front national, en 2012 :


Cette année encore, StreetPress et Libération ont documenté des connexions
entre RN et radicaux (néofascistes, identitaires, royalistes), que ce soit
à travers le Rassemblement national de la jeunesse (RNJ) ou à travers les
collaborateurs d’élus.

Cette porosité est plus grande encore chez Reconquête qui, dès sa création
en 2021, a réuni plusieurs chapelles de l’extrême droite la plus radicale,
militants issus de l’Action française, de la Ligue du Midi ou de Génération
identitaire (GI), comme Mediapart l’a dévoilé. Parmi eux, deux piliers des
identitaires passés par le RN avant de devenir cadre ou élu de Reconquête :
Damien Rieu, cofondateur de GI qui avait occupé, avec d’autres identitaires,
le chantier de la mosquée de Poitiers en 2012 ; Philippe Vardon, cofondateur
du Bloc identitaire au passé chargé.

La présence de radicaux a éclaté au grand jour en décembre 2021 lors
du meeting de lancement de la campagne présidentielle d’Éric Zemmour,
où des membres des Zouaves Paris (groupuscule néonazi héritier du GUD)
ont agressé des militants de SOS Racisme, avant d’être remerciés par le
service d’ordre.

Cette convergence s’est aussi illustrée dans les manifestations qui ont eu
lieu à Saint-Brevin (Loire-Atlantique) ou Callac (Côtes-d’Armor) contre
des projets de centres d’accueil de demandeurs d’asile, où se côtoyaient
membres de Reconquête, de Civitas et de groupuscules violents.

Un même socle idéologique d’extrême droite
---------------------------------------------------

La porosité ne se limite pas aux militants, elle concerne aussi le socle
idéologique. "Ce qui lie cette famille politique, c’est son idéologie
», rappelait, sur le plateau d’Arrêt sur images, la politiste Bénédicte
Laumond, qui étudie les radicalités d’extrême droite en France et en
Allemagne.  Illustration 3 Affiche du FNJ (Front national de la jeunesse)
lors de la campagne présidentielle en 2017.

Si les moyens d’organisation, le recours ou non à la violence varient,
soulignait la chercheuse, l’idéologie reste la même, qui repose sur deux
piliers : "l’ethnocentrisme"– l’idée que la nation est un peuple
homogène, avec un groupe dominant (souvent culturel ou ethnoracial) dont sont
exclues les minorités ; et "l’autoritarisme", c’est-à-dire le soutien
à des politiques autoritaires, qui se déploie avant tout contre les minorités.

Les extrêmes droites partisane et groupusculaire partagent le même ciment
idéologique (le rejet de la société multiculturelle, de la présence des
immigrés et de l’islam en Europe), et certains slogans : comme le fameux
"On est chez nous !", entendu autant dans les meetings de Marine Le Pen
que dans les expéditions de groupes violents ; la "préférence nationale
», rebaptisée "priorité nationale"au RN ; le "racisme anti-Blanc
» ou "anti-Français"; ou encore la théorie complotiste du "grand
remplacement", popularisée par les identitaires et Éric Zemmour, brandie
par des terroristes d’extrême droite et aujourd’hui reprise à son compte
par le président du RN Jordan Bardella.

Cette convergence des slogans a pu s’observer cette semaine concernant la
mort du jeune Thomas à Crépol (Drôme) :

**Parler d’Ultradroite, c’est donc gommer le fait que ces groupuscules
violents reposent sur un même socle idéologique d’extrême droite**.


**L’ambiguïté du RN, l’absence de condamnation de Reconquête**
-----------------------------------------------------------------

C’est la raison pour laquelle Marine Le Pen se refuse toujours à qualifier
ou condamner l’idéologie de ces groupes.

En décembre 2022, lorsque des attaques émanant de groupuscules violents se
multiplient, la cheffe de file du RN est contrainte de s’en dissocier, mais
dans son courrier demandant leur dissolution, elle désigne de manière floue
les "groupuscules extrémistes", "quel que soit leur profil politique".

Autre exemple : en mai 2023, lors du défilé annuel des groupuscules
néofascistes à Paris, elle condamne le type d’organisation, le recours à
la violence (parlant d’un défilé "provocateur", "inadmissible",
"en uniforme", "masqué"), mais jamais l’idéologie.

Plus ambigu encore : le positionnement du RN à l’égard de Génération
identitaire, groupe connu pour ses actions d’agitprop flirtant avec la
légalité. En 2018, Marine Le Pen rend "hommage" à leur action antimigrants
dans les Alpes. En 2021, lorsque le ministère de l’intérieur annonce la
dissolution du groupe – en raison de son idéologie incitant à la haine et
à la violence envers les étrangers et les musulmans –, la cheffe de file du
RN le défend au nom de la liberté d’expression – son porte-parole, Julien
Odoul, le qualifiera même de "lanceur d’alerte". Au fil des années,
les identitaires ont constitué un vivier de cadres et d’idées pour le RN.

De son côté, le parti d’Éric Zemmour se refuse carrément à condamner
les violences de ces groupes. 

Cette semaine encore, questionnée par RTL sur
l’expédition punitive de militants d’extrême droite à Romans-sur-Isère,
la vice-présidente de Reconquête Marion Maréchal a ironisé sur cette 
"soi-disant menace de la gigadroite", des "zozos qui ont des méthodes qui
évidemment ne sont pas les [siennes]", dont la manifestation n’aboutirait
"à aucune violence réelle".

La nécessité de distinguer les chapelles d’extrême droite
--------------------------------------------------------------

Les outrances de Reconquête et les violences de ces groupuscules ont permis
à Marine Le Pen de parfaire sa stratégie de "dédiabolisation", en
expliquant que l’extrême droite, ce serait eux, et non son parti. 

Le RN bataille d’ailleurs depuis des années pour ne plus être qualifié d’"
extrême droite"par les médias, les universitaires et même le ministère
de l’intérieur à l’occasion des élections – une bataille qu’il a
encore perdue devant le Conseil d’État en septembre.

"Le RN correspond au courant national-populiste de l’extrême droite,
qui apparaît dans les années 1880. Il n’est certes pas nazi, mais sous
ce prétexte, dire qu’il n’est pas d’extrême droite, c’est violer la
sémantique, l’histoire et la logique", souligne Nicolas Lebourg.

Pour autant, la qualification d’"extrême droite"ne peut à elle seule
dépeindre la diversité de la mouvance. Et mettre au même niveau des partis
politiques comme le RN, des groupuscules violents, des radicaux ne passant
pas à l’action et des groupes préparant des projets terroristes serait
évidemment malhonnête. C’est pourquoi il est nécessaire de parler d’"
extrêmes droites", et de qualifier les courants idéologiques (identitaires,
nationalistes-révolutionnaires, néofascistes, néonazis, catholiques
intégristes, etc.) et les modes d’action (politique, violent, terroriste).


.. _usage_terme_extremes_droites:

**Mediapart et l’usage du terme `Ultradroite`**
------------------------------------------------------

Mediapart a commencé à employer le terme :term:`Ultradroite` en 2013, à l’occasion 
de la mort du militant antifasciste Clément Méric, tué par des skinheads, et 
lors des violences émaillant les manifestations contre le mariage pour tous (ici et là).

Au fil des années, nous avons eu à la rédaction de nombreux débats sur l’emploi 
de ce vocable, essentiellement policier, employé comme fourre-tout par les médias.

Aujourd’hui, cette manière de nommer les choses ne nous paraît plus correspondre 
à la réalité et sa complexité. 
La mouvance d’extrême droite est faite d’une multitude de chapelles, en constante 
recomposition, avec une porosité et des alliances ponctuelles importantes, 
comme nous l’avons raconté. 

Impossible, dès lors, de tracer une ligne étanche entre extrême droite partisane 
et ultradroite violente.

**Considérant que l’époque nécessite le maximum de clarté, Mediapart parlera donc 
désormais d’"extrêmes droites"**, en distinguant les modes d’action – politique, 
violent ou terroriste.


