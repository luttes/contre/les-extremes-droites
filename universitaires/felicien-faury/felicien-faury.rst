.. index::
   pair: Universitaire; Félicien Faury


.. _felicien_faury:

=====================================================
**Félicien Faury**
=====================================================

Bio
======

Félicien Faury est sociologue et politiste, chercheur postdoctoral au CESDIP 
(Centre de recherches sociologiques sur le droit et les institutions pénales).



Livres 2024
================

- :ref:`electeurs_ordinaires_2024`


Interventions 2024
=======================

:ref:`antiracisme_2024:faury_2024_04_27`
