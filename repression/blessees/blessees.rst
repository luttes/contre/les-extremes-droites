.. index::
   pair: Armes de guerre ; Blessé(e)s

.. _blessees:

======================================================================================================
Mort(e)s et blessé(e)s
======================================================================================================

.. seealso::

   - https://twitter.com/davduf
   - https://fr.wikipedia.org/wiki/Lanceur_de_balles_de_d%C3%A9fense#Liste_de_bless%C3%A9s_et_de_victimes

.. toctree::
   :maxdepth: 3

   2020/2020
   wikipedia/wikipedia
