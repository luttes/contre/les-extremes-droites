.. index::
   pair:Wikipedia ; Blessé(e)s

.. _blessees_wikipedia:

======================================================================================================
Liste des Mort(e)s et blessé(e)s sur Wikipedia
======================================================================================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Lanceur_de_balles_de_d%C3%A9fense#Liste_de_bless%C3%A9s_et_de_victimes


.. contents::
   :depth: 3


Introduction
=============

Une personne est morte et au moins 23 autres (la plupart lors de manifestations)
avaient perdu l'usage d'un œil depuis que l'utilisation de ce type d'arme par la
police avait été généralisée en France et l'année 2010 (chiffres non mis à jour).

Officiellement, dans la plupart des cas, le LBD a été utilisé par les policiers
dans des Zones urbaines sensibles, ou dans des manifestations.

