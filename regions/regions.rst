
.. index::
  pair: France; Antifascisme

.. _antifascisme_regions_france:

==================================================
Régions
==================================================

.. toctree::
   :maxdepth: 5


   midi_pyrenees/index
   rhone_alpes/index

