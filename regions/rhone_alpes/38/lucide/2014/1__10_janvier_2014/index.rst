

.. index::
   pair: LUCIDE; 2014
    
.. _lucide_10_janvier_2014:   
   
======================================================================================
Vendredi 10 janvier 2014 FN = F’Haine = extrême droite ! Le Pen, ni ici, ni ailleurs !
====================================================================================== 

:download:`Tract au format PDF <14_01_2014_appel_contre_FN.pdf>` 

- Non à la préférence nationale
- Non au déremboursement de l’IVG
- Non aux discriminations
- Non à la casse des droits sociaux

- Pour l’égalité des droits
- Pour la justice sociale
- Pour les libertés
- Pour les solidarités


Rassemblement unitaire Vendredi 10 janvier 2014 à 18 h
Place Pasteur - Grenoble


Premiers signataires 
====================

ADDIRP38, Alternatifs, Alternative Libertaire, Association des Libres Penseurs,
Amazigh, CIIP, Comité de soutien aux réfugiés Algériens, Comité Traite négrière/
Esclavage, FASE, FSU, GA, GU, JC, Iran Solidarités, LDH Isère, LIFPL, Militants
citoyens, Mouvement de la Paix, MJS, NPA, Osez le Féminisme, PAG38, PAS 38,
PCOF, PG, RLF Grenoble-Grésivaudan-Voiron, Solidaires, SOS Racisme, UD-CFDT,
UD-CGT, UNEF, UNL



