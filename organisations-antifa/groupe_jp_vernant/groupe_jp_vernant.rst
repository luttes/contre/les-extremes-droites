.. index::
   ! Groupe JP Vernant

.. _groupe_jp_vernant:

========================================================================
**Groupe Jean-Pierre Vernant**
========================================================================

.. seealso::

   - https://twitter.com/Gjpvernant
   - https://fr.wikipedia.org/wiki/Jean-Pierre_Vernant


*Si le *nous* n'existe plus, nous n'existons plus**

.. toctree::
   :maxdepth: 4

   articles/articles
