
.. index::
   pair: Rencontres Voiron; 2019
   pair: RLF ; 2019


.. _rencontres_voiron_2019:

=============================================
11e rencontres de Voiron le 12 janvier 2019
=============================================


:download:`tract-rlf-nov2018_recto.pdf`
:download:`tract-rlf-nov_2018_verso.pdf`


.. contents::
   :depth: 3

Samedi 12 janvier 2019 - à Voiron - salle des fêtes
======================================================

dès 15 h - Stands

Une trentaine d’associations présentent leurs actions, proposent des livres ou
des documents et des objets en vente solidaire.

liste établie au 6 déc actualisation sur rlf38.org

ANACR amis de la Résistance

- Antigone Café bibliothèque librairie
- ATTAC 
- Capjpo Euro-Palestine
- Coord BDS
- Coord Center parcs
- CCFD Terre solidaire
- la CGT
- CIIP Information Inter Peuples 
- CISEM soutien aux migrants
- Ciné Repaire
- Confédération paysanne
- Front social grenoblois
- CRIC Communiquer Relayer Informer Combattre
- DAL38 Droit au logement
- Libraire Colibri
- Librairie Nouvelle
- Mouvement de la Paix 38
- Nuestra America 
- Patate Chaude - Patio solidaire
- Réseau Éducation Sans Frontières RESF Voiron
- SGAP38 solidarité artisans palestiniens
- le Tamis
- Travailleurs sans-papier CGT
- et d’autres encore...
- Ligue des Droits de l’Homme (Grenoble Métropole
- LIFPL Ligue internationale des femmes pour la Paix et la Liberté


17h  débat avec  Claire Rodier
================================

Juriste, directrice du GISTI (Groupe d’information et de soutien des immigrés),
co-fondatrice du réseau euro-africain Migreurop.
Elle travaille plus particulièrement sur les politiques européennes
d’immigration et d’asile.
Elle a participé à de nombreuses publications sur ces thèmes, collaboré àl’Atlas
des migrants en Europe (Armand Colin, rééd. 2012) et coordonné, avec Emmanuel
Terray, l’ouvrage collectif Immigration, fantasmes et réalités (La Découverte, 2008).

Dernier ouvrage : *Migrants & réfugiés: réponse aux indécis, aux inquiets et aux réticents*

18h30  rencontres et repas salades, plat chaud, gâteaux, boissons
=====================================================================

20h
-----

Débat avec Ugo Palheta sociologue, maître de conférences en sciences de
l’éducation à l’Univ. Lille 3, et chercheur associé au GRESCO.
Par ailleurs directeur de publication de la revue Contretemps
(https://twitter.com/SRContretemps, http://www.contretemps.eu/)

Ses recherches portent en particulier sur les modes de production et de
légitimation de l’inégalité (de classe, de genre et de race).

Il travaille en outre sur le danger fasciste, en particulier sous l’angle de
l’analyse des effets politiques des contre-réformes néolibérales, du processus
de durcissement autoritaire des États capitalistes, de l’aiguisement des
nationalismes sous l’effet de l’intensification du racisme.

Dernier ouvrage: *La possibilité du fascisme*. France : trajectoire du désastre.
Un livre pas (seulement) sur l’extrême droite.

Il faut prendre au sérieux cette dernière, mais il importe tout autant de
replacer sa résurrection et son renforcement dans le processus historique
de radicalisation de la classe dirigeante française dans son ensemble.

fin à 23h - libre participation aux frais
---------------------------------------------


Les intervenants , leur dernier livre et leur argumentaire
=============================================================

Migrants & réfugiés : réponse aux indécis, aux inquiets et aux réticents. Éd. La Découverte, 2018 Claire RODIER
-----------------------------------------------------------------------------------------------------------------

L’arrivée en grand nombre de réfugiés et de migrants en Europe, à partir de
2015, ainsi que les nombreuses morts en Méditerranée, dont celle, très
médiatisée, du petit Aylan Kurdi, ont souvent ému et *bousculé* la population
européenne.

Toutefois, après une première phase d’accueil, un discours officiel
de défiance, voire hostile aux migrants s’est progressivement imposé sous
la pression de l’extrême droite européenne, les transformant, ainsi que
ceux qui leur portent assistance, en ennemis à combattre: en témoignent
notamment les attaques contre le bateau humanitaire Aquarius en 2018.

Malgré une baisse spectaculaire du nombre d’entrées irrégulières sur le
territoire européen, les inquiétudes et les réticences s’expriment chez
ceux qu’un élan de solidarité avait poussés à ouvrir leurs portes aux
migrants, et de nombreuses questions émergent :

- quelle différence entre réfugiés et migrants ?
- Combien sont-ils ?
- La France et l’Europe ont-elles la capacité d’accueillir ces migrants, compte
  tenu de la crise économique ?
- Les murs servent-ils à quelque chose ?
- Qu’est-ce qu’un hotspot ?
- Qu’est-ce que le délit de solidarité ?
- Ne vaudrait-il pas mieux les aider à rester chez eux ?

C’est pour répondre sans tabou à ces interrogations légitimes, et à bien
d’autres, que ce petit livre a été conçu.

Version papier : 4,90 €
Version numérique : 3,49 €

La possibilité du fascisme. France : trajectoire du désastre. Éd. La Découverte, 2018 Ugo PALHETA
===================================================================================================

- http://www.contretemps.eu/democraties-capitalistes-autoritarisme-fascisme/

- Mouvement réactionnaire de masse contre l’égalité des droits ;
- migrants enlevés, tabassés et laissés pour mort par des milices à Calais ;
- large diffusion de thèses réactionnaires, xénophobes et islamophobes ;
- intensification du quadrillage répressif des quartiers populaires et
  violences policières impunies ;
- manifestations interdites et criminalisation croissante de toute contestation ;
- scores inégalés du Front national à toutes les élections depuis 2012.

Sous des formes disparates et encore embryonnaires, mais dont la seule
énumération dit le pourrissement actuel de la politique, c’est le fascisme
qui fait retour.

Et celui-ci s’annonce non comme une hypothèse abstraite mais comme une
possibilité concrète.
Pourtant, la possibilité du fascisme est généralement balayée d’un revers
de main par les commentateurs : comment la République française, patrie
autoproclamée des droits de l’homme, pourrait-elle engendrer le monstre fasciste ?

La France ne s’est-elle pas montrée *allergique* au fascisme tout au long du
XXe siècle, comme le prétendent certains historiens français ?
Le Front national n’a-t-il pas renoncé au projet ultranationaliste, raciste et
autoritaire qui le caractérisait depuis sa création ?
N’assiste-t-on pas au renouveau du capitalisme français sous les auspices
d’un jeune président réalisant enfin les *réformes* prétendument nécessaires ?

C’est à démonter ces fausses évidences que s’attache ce livre, scrutant ainsi
la trajectoire d’un désastre possible, enraciné dans la triple offensive,
néolibérale, autoritaire et raciste, dont Emmanuel Macron est la parfaite
incarnation, mais un désastre résistible, pour peu que le danger soit reconnu
à temps et qu’émerge un nouvel antifascisme, capable de mener de front le
combat contre l’extrême droite et celui contre les politiques destructrices
qui favorisent son ascension.

Version papier : 17 €
Version numérique : 11,99 €


L’organisateur: RLF Isère  - Réseau de Lutte contre le Fascisme (ex Ras l’front)
==================================================================================

Nos combats :

- LA LIBERTÉ DE CIRCULER, c’est un droit fondamental comme le dit la
  déclaration universelle des droits humains. Au lieu d’un contrôle policier
  toujours plus inhumain et vain, au lieu de barrières criminelles qui
  favorisent les trafiquants, construisons des DES PONTS, PAS DES MURS.

- LE RACISME: la peur du *grand remplacement*, fantasmé avec l’aide de
  medias complaisants, engendre un racisme dont le seul but est de faire
  perdurer les rapports de domination parfois même avec la complicité des
  institutions de l’ÉTAT.

- Le néolibéralisme s’accommode fort bien, (au Brésil comme ailleurs) des
  RÉGIMES AUTORITAIRES. *Plutôt Hitler que le Front populaire* pourrait
  encore être son slogan. En France, notre monarchie présidentielle n’a
  déjà plus que l’apparence d’une démocratie. De dérive en dérive, les
  classes dominantes pourraient bien faire le lit des fascistes.

- CONTRE LES LOIS LIBERTICIDES. On ne compte plus les lois restreignant
  les libertés sous prétexte de sécurité et qui en fait, sont souvent
  promulguées pour rendre impossible toute contestation de l’ordre
  établi ou du *secret* des affaires.

- En bref, nous sommes POUR L’ÉMANCIPATION de tout un chacun.
  C’est à dire que chacun- e puisse exercer son libre arbitre et que
  cesse l’exploitation sans fin(s)  : égalité et justice dans le respect
  des autres et de notre environnement.

Agissons ensemble pour stopper l’extrême droite avant qu’il ne soit trop tard.

rlfvoiron38@no-log.org

http://rlf38.org



Débats
========

.. toctree::
   :maxdepth: 3

   claire_rodier/claire_rodier
   ugo_pahleta/ugo_pahleta
