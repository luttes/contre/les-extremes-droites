
.. index::
   pair: RLF ; Voiron


.. _rlf_rhone_alpes:

=======================================
RLF Voiron
=======================================

.. seealso::

   - https://www.isere-antifascisme.org/
   - http://rlf38.org/
   - https://www.youtube.com/channel/UCF23sqGobWsXzqn10VvgcFA/featured


::

    RLF Voiron
    MDA, 2, place Stalingrad
    38500 Voiron

- courriel: rlfvoiron38@no_log.org


.. toctree::
   :maxdepth: 3

   rencontres/rencontres
