
.. index::
  pair: VISA; Antifascisme
  ! VISA

.. _visa:

=========================================================
Vigilance et Initiatives Syndicales Antifascistes (VISA)
=========================================================


.. seealso::

   - http://www.visa-isa.org


.. contents::
   :depth: 3

Adresse
========

::

    Vigilance et Initiatives Syndicales Antifascistes
    80/82 rue de Montreuil -
    75011 PARIS

Courriel
========

:courriel: assovisabis@gmail.com


Actions
=======

.. toctree::
   :maxdepth: 3

   actions/actions

Qui sommes nous ?
=================

VISA regroupe des militant(e)s de la FSU, de la CGT, de la CFDT et de
SOLIDAIRES (adhérent(e)s de sections d'entreprises, élu(e)s de syndicats
nationaux ou représentant(e)s de fédérations professionnelles) qui,
depuis 1996, recensent, analysent, dénoncent, les incursions de l’extrême
droite et plus particulièrement du Front National sur le terrain social.

VISA a pour ambition d’être un outil d’information et de réflexion pour
toutes les forces syndicales qui le souhaitent afin de lutter collectivement
contre l'implantation et l'audience de l'extrême droite dans le monde du
travail.

Dix ans après l'électrochoc de 2002 - avec l'arrivée de Le Pen au deuxième
tour de la présidentielle - les 17,9 % de Marine Le Pen à l’élection
présidentielle de 2012,  confirmés dans de nombreuses régions aux législatives,
montre que la gangrène fasciste s'est enkystée durablement dans la société
française. L’élection de deux députés FN en est la concrétisation.

Ces résultats ne peuvent donc aucunement  laisser indifférents les
syndicalistes que nous sommes. Les « valeurs » et l’idéologie portées par
le FN, totalement aux antipodes des idéaux de solidarité et de progrès
que nous défendons, trouvent donc un réel écho dans une fraction importante
de l’électorat.
En gommant les aspects les plus ultra-libéraux de son programme initial,
le Front National tient maintenant un discours qui se veut social.
Ce faisant il cherche à tromper les salariés et les couches populaires.

Nous, syndicalistes, avons une responsabilité particulière pour dénoncer
les pseudos solutions du FN qui consistent à dresser les uns contre les
autres  les précaires, les immigrés, les chômeurs et ceux qui ont un
emploi. Le devoir des syndicalistes face au venin de la division distillé
par le FN est de défendre,  de façon intransigeante, dans notre propagande
et nos actions, la solidarité de tout le salariat quelque soit son origine
ou son statut.

Ce combat antifasciste doit être pris en charge par les grandes
confédérations syndicales, les syndicats nationaux mais aussi les
structures syndicales de base.

Chaque fois que cela est possible, cette contre offensive syndicale
doit se faire dans l'unité la plus large.

L’association VISA est un outil qui se veut partie intégrante de ce combat.

Réunissant des syndicalistes de différentes organisations (CGT, CFDT,
SOLIDAIRES, FSU) et de différents secteurs, elle entend y contribuer :

- En livrant un maximum d'information sur les dangers et le développement
  de l'extrême droite en France, en Europe et dans le monde ; ainsi que
  sur les « passerelles » jetées avec certains partis de la droite dure.
- Par des analyses servant à démystifier les pseudos propositions sociales
  de l’extrême droite.
- En relayant sur son site toutes les prises de positions, actions des
  organisations syndicales contre l'extrême droite.
- En dénonçant toutes les discriminations racistes, sexistes, homophobes
  au sein et hors des entreprises.
- En se faisant l'écho de toutes les actions de solidarité avec les sans
  papiers.
- En aidant, par des outils adaptés (journées de formation, brochures,
  affiches etc..), les équipes syndicales à se former et se mobiliser
  pour démystifier les soi-disant propositions sociales de l’extrême
  droite, dans les discussions nécessaires avec l’ensemble des salariés.

Vigilance et Initiatives Syndicales Antifascistes, V.I.S.A., Août 2012

