
.. index::
   pair: Antifascisme; Manifeste antifasciste européen


.. _manifeste_antifasciste_europeen_2013_ref:

=========================================================
Manifeste antifasciste européen (mars 2013)
=========================================================


.. seealso:: http://www.visa-isa.org/node/14385



Introduction
=============

VISA a décidé de signer le présent manifeste antifasciste européen, aux 
cotés d’élus, de syndicalistes, d’intellectuels et de militants politiques 
européens. 

Déjà, en un mois, plus de deux mille signatures venant d’une quinzaine 
de pays ont été récoltées.

Face à la montée fulgurante du fascisme en Grèce, dont notre site s’est 
fait l’écho ces derniers mois, VISA s’engage dans une démarche unitaire, 
à l’échelle européenne.

Nous ne pouvons rester les bras croisés face aux agissements de 
l'extrême droite: agressions physiques sur les travailleurs étrangers 
(de l'infirmière au manœuvre ou au maraicher), sur les personnes LGBT, 
agressions et menaces sur les syndicalistes (Moisis Litsis, journaliste 
et porte-parole d'une grève...), sur les personnes malades ou handicapées, 
et sur les militants de gauche.

De plus, l'Aube Dorée n'avance pas masquée. Outre ses agissements, son 
programme est clair: la société sera en coupe réglée, la raison du plus 
fort en sera la loi.

Nous appelons les syndicats et les associations à prendre contact avec 
nous pour signer ce manifeste et, dans un second temps, participer aux 
actions de solidarité qui en découleront.

VISA participera également au Forum Social Mondial de Tunis et animera 
un atelier « syndicalisme et antifascisme » le mercredi 27 mars de 16h à 18h30.

Manifeste antifasciste européen
===============================

.. seealso::

   - :ref:`manifeste_antifasciste_europeen_2013`
   
