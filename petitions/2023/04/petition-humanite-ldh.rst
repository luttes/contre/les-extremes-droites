.. index::
   pair: Pétition ; LDH (2023-04)

.. _petitions_ldh_2023_04:

=================================================================
2023-04 **On ne touche pas à la Ligue des droits de l’homme !**
=================================================================


On ne touche pas à la Ligue des droits de l’homme !
Déjà 10 000 signataires... et vous ?

Mardi 11 avril 2023
======================

Une semaine après les menaces de Gérald Darmanin contre la Ligue des
Droits de l'Homme, l'Humanité a pris l'initiative d'un appel en soutien
à l'association signé par 1000 personnalités.

En un jour, depuis son lancement mardi 11 avril à 19 heures, 9 000
personnes supplémentaires ont signé cet appel.

Rejoignez-les !


Devant les sénateurs, mercredi 5 avril, le ministre de l’Intérieur Gérald
Darmanin a gravement mis en cause la vocation et l'indépendance de la
Ligue des droits de l’Homme (LDH), coupable à ses yeux de défendre les
libertés publiques contre les dérives de la politique du maintien de
l'ordre, et l'a menacée en rétorsion de couper l'aide publique qui lui
revient.

« Je ne connais pas la subvention donnée par l'Etat, a-t-il affirmé,
mais ça mérite d'être regardé dans le cadre des actions qui ont pu être
menées. »

Cette intimidation à peine voilée est d’une gravité extrême concernant
une association centenaire, reconnue pour son action exemplaire dans
la protection des libertés et le respect de l’Etat de droit.

Fondée en 1898 pour combattre l’injustice antisémite faite au capitaine
Dreyfus, la LDH a été de tous les grands combats de la République.

Laïcité, lutte antifasciste et antiraciste, défense des droits des étrangers...

Elle s'est depuis toujours mobilisée pour préserver et promouvoir les
droits de chaque être humain – sa raison d’être – dans toutes les sphères
de la vie publique.

Une défense exigeante de notre démocratie
=============================================

Depuis plusieurs semaines, elle mène un travail d’observation des pratiques
policières avec le souci d'œuvrer à la désescalade des violences, afin
de garantir l’ordre républicain et le droit de manifester.

C’est cette défense exigeante de notre démocratie qui est visée par le
sous-entendu inacceptable de Gérald Darmanin.

Les subventions sont indispensables pour garantir l’indépendance des
associations et les préserver de l’arbitraire d’un pouvoir.

Les remettre en cause pour des arrière-pensées politiques est une manière
de faire taire les contrepouvoirs et d’éteindre le débat public.

Cette pratique, habituelle des régimes autoritaires, doit alerter tous
les défenseurs de notre système démocratique. Par cet appel, nous réaffirmons
notre vigilance et exigeons que cessent ces menaces visant la Ligue des
droits de l’homme.


