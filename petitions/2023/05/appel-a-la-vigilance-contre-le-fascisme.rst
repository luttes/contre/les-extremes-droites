.. index::
   pair: Pétition ; Appel à la vigilance contre le fascisme (2023-05)

.. _petition_2023_05:

=================================================================
2023-05 **Appel à la vigilance contre le fascisme**
=================================================================

- https://www.change.org/p/appel-%C3%A0-la-vigilance-contre-le-fascisme


À l’appel de Renaud, Yvan le Bolloc’h, Philippe Poutou, Guillaume Meurice,
de nombreux artistes et personnalités :

Quand l’Etat devient laxiste avec la montée du fascisme, que des dirigeants
cautionnent par un silence complice des exactions de plus en plus graves,
que des élus détournent le regard de la montée de la peste brune, il est
du devoir du peuple de résister !

Le 6 mai 2023, deux jours avant la commémoration de résistants et déportés,
la célébration de la victoire contre le régime hitlérien, une marche
terrifiante de néo-nazis est autorisée par la préfecture de police en
plein Paris !

C’est intolérable !

**Les autorités ont revendiqué la liberté d’expression mais le racisme
n’est pas une opinion, c’est un délit.**

Alors que des cartons rouges ou des casseroles sont interdits ou confisqués,
des drapeaux noirs arborant la croix celtique (nostalgie néofasciste
d’Occident, d’Ordre Nouveau ou du GUD) sont tolérés.
Le soir même, un concert de groupes se revendiquant aryens est organisé
dans la salle Simone Veil, ministre rescapée de la Shoah, où furent
constatés des saluts nazis.

À chaque fois les réactions du gouvernement sont tardives et timides.

Alors que Pétain est scandaleusement réhabilité, que le discours public
se fait ambigu, qu’un élu de la majorité trouve Marine Le Pen trop molle,
que l’extrême droite est banalisée et mise dans le même sac que des
partis de gauche prônant l’exact contraire des idées racistes, que des
tribunes télévisées sont accordées à des militants condamnés pour
provocation à la haine raciale, il est du devoir du peuple d’exprimer
fortement sa très ferme révolte contre ces très dangereuses dérives qui
nous préparent à un avenir extrêmement sombre.

N’oublions jamais les leçons de l’Histoire !

Le 10 mai, Yannick Morez, maire de Saint-Brevin-les-Pins, annonce sa
démission devant sa maison et ses véhicules incendiés parce qu’il a
manifesté un devoir d’accueil humaniste à des étrangers.

Il a dénoncé une absence totale de soutien de l’Etat. Inquiet pour sa
femme et ses enfants, il va quitter sa commune après 15 ans de service
dans sa mairie. Le RN a délibérément refusé d’exprimer la moindre compassion.

Cette accumulation de faits monstrueux nous appelle à hurler notre colère !
Nous sommes là pour Yannick Morez, très touchés par son combat et sa
décision, en total soutien et solidarité…

Quand l’État devient faible, qu’il renonce, capitule et s’habitue à
l’intolérable, nous demeurons vigilants et responsables à sa place.

Le fascisme ne passera pas !

Cette lettre ouverte est déjà signée par des artistes et personnalités suivantes :

Renaud, Monique Pinçon-Charlot, Yvan le Bolloc’h, Philippe Poutou, Xavier Mathieu,
Guillaume Meurice, Laurent Binet, Laurence de Cock, David Hugla, Benoit Delépine,
Didier Super, Bruno Solo, Stéphane Brizet, Mathilde Larrère, Pierre-Emmanuel Barré,
Cédric Herroux, Lolita Séchan, Laurence de Cock, Zazon Castro, Pia Moustaki,
Corinne Masiero, Denis Robert, Marcel et son orchestre, Sanseverino…

