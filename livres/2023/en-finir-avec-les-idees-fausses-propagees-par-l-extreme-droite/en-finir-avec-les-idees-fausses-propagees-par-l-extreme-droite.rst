.. index::
   pair: Livre; En finir avec les idées fausses propagées par l'extrême droite (2023)
   pair: Vincent Edin  ; En finir avec les idées fausses propagées par l'extrême droite (2023)

.. _idees_fausses_2023:

========================================================================================================================================================
2023 **En finir avec les idées fausses propagées par l'extrême droite** par Vincent Edin 
========================================================================================================================================================

- https://editionsatelier.com/boutique/accueil/378-en-finir-avec-les-idees-fausses-sur-l-extreme-droite--9782708254213.html


.. figure:: images/recto.webp

Auteur:  Vincent Edin
=====================================================

Vincent Edin est journaliste, auteur de plusieurs ouvrages dont 
"Déconstruire les mensonges et les raccourcis pour résister à l’extrême droite !".



Description
=============

2017. 2022. Deux élections présidentielles de suite que l'extrême droite se 
retrouve au second tour des élections présidentielles. 

Aujourd'hui aux portes du pouvoir, elle récolte les fruits d'une décennie 
d'une banalisation de son discours rendue possible grâce à la perméabilité 
des médias et des politiques sur ses sujets de prédilection comme l'immigration 
ou l'insécurité, tout en investissant de plus en plus les thématiques culturelles, 
du genre, ou de l'écologie. 

À l'heure où l'extrême droite a plus que jamais intégré le fonctionnement 
des institutions et où son discours se montre de plus en plus menaçant pour 
la démocratie, il semble indispensable de préciser les contours de sa pensée 
pour mieux la contrecarrer.

Ces idées, alors que les différentes crises s’approfondissent, ne cessent de 
progresser en France et en Europe. Loin d’être l’apanage des partis extrémistes, 
elles se glissent partout sur l’échiquier politique, parviennent à monopoliser 
le débat public, touchent de larges fractions de la population et menacent 
la démocratie. 

Ce livre contrecarre une à une les idées qu’elle véhicule, telles que : 

- "Si on renvoyait les étrangers chez eux, le chômage disparaîtrait", 
- "Il faut défendre les racines chrétiennes de l'Europe", 
- "Les féministes n'aiment pas les hommes", 
- "Les universités sont gangrénées par l'islamo-gauchisme", 
- "Les Khmers verts veulent nous empêcher de vivre", …

**En réfutant une à une, sur la base de données solides, quatre-vingts idées 
fausses propagées par l’extrême droite**, ce livre donne à chacun les arguments 
nécessaires pour répliquer à ces poncifs basés sur des mensonges, des omissions, 
ou sur la falsification de l’Histoire. 

En laissant croire que l’égalité des êtres humains n’est pas une chance mais 
une menace, l’extrême droite attise la lutte de tous contre tous, la haine 
de l’autre et de soi. 

Cet ouvrage est un outil indispensable pour tous ceux qui veulent résister 
à la tentation autoritaire et faire vivre la démocratie.


Presse 2024
================

Faut-il être extrême pour se faire entendre ? Avec Vincent Edin
----------------------------------------------------------------------

- https://www.vlanpodcast.fr/episodes/305-faut-il-etre-extreme-pour-se-faire-entendre-avec-vincent-edin


Video 2023 Comment se réarmer face au danger de l’extrême droite ?
======================================================================

- https://www.youtube.com/watch?v=wi8DQ9bPAws&t=1s&ab_channel=BLAST%2CLesouffledel%27info

.. youtube:: wi8DQ9bPAws


