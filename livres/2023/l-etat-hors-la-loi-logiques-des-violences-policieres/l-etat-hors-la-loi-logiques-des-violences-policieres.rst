

.. _etat_hors_loi_2023_09_14:

====================================================================================================
2023-09-14 **L'État Hors-La-Loi - Logiques Des Violences Policières** par Arié Alimi
====================================================================================================

- https://www.librairiesindependantes.com/product/9782348078217/


.. figure:: images/couverture.webp

Résumé
===========

La multiplication récente des violences policières, des morts et des
blessés qu'elles ont entraînés, a rappelé à quel point l'usage de la
force est corrélé au pouvoir d'État. Pour autant, ces violences restent
largement impensées, généralement considérées comme la conséquence
de contradictions internes à la gestion de l'ordre néolibéral. 

Or les violences qui ont conduit à la mort de Nahel M., à celle de Rémi Fraisse,
à celle de Cédric Chouviat, comme celles qui ont consisté à mettre à
genoux les lycéens de Mantes-la-Jolie ou à mutiler des gilets jaunes n'ont
ni les mêmes modalités ni les mêmes rationalités.

Fondé sur l'analyse des dossiers judiciaires auxquels l'auteur a eu accès, 
ce livre montre que les armes, les techniques, les pratiques et les objectifs, 
ainsi que les réactions politico-médiatiques et les traitements judiciaires diffèrent
selon que les violences ciblent une expression politique, l'exercice d'une
liberté de circulation ou la simple appartenance ethno-raciale.

Discipliner, punir, instaurer ou restaurer un rapport de domination, territorialiser
l'espace public, l'espace privé, les flux de circulation et, dans les
cas les plus extrêmes, exprimer une violence pure – celle de l'antique
pouvoir de vie et de mort –, telles sont les différentes fonctions des
violences policières. 

Cette distinction permet de mieux saisir les rapports
de pouvoir qui s'expriment entre l'État et la population et entre la police
et des groupes sociaux déterminés. 

Elle offre aussi des prises pour tenter de
répondre à une question plus fondamentale : la violence est-elle constitutive
du pouvoir, un moyen de son exercice ou une condition de sa possibilité ?


Caractéristiques
==================

EAN13
    9782348078217
Éditeur
    La Découverte
Date de publication
    14 septembre 2023
Nombre de pages
    232
Dimensions
    20,6 x 14,2 x 2,1 cm
Poids
    279 g
Langue
    fre 


Sur les réseaux sociaux
==========================

2024-07-24 Macron vient d'instaurer un nouvel état d'exception
------------------------------------------------------------------

- https://x.com/AA_Avocats/status/1815989430772957226

Macron vient d'instaurer un nouvel état d'exception. 
Non prévu par la loi ou la constitution. En ne désignant pas de premier 
ministre, dans un parlement sans majorité, il devient seul décisionnaire. 
"C'est le souverain qui décide de l'état d'exception" Carl Schmitt #Macron20h

