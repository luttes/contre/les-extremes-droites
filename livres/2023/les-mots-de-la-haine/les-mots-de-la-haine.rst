
.. index::
   pair: Kersimon; Les mots de la haine (2023-04-12)
   pair: Livre; Les mots de la haine (2023-04-12)

.. _kersimon_2023_04_12:

=============================================================================================================
**Les mots de la haine : Glossaire des mots de l’extrême droite** par Isabelle Kersimon
=============================================================================================================

- https://ruedeseine.fr/livre/les-mots-de-la-haine-glossaire-des-mots-de-lextreme-droite/

L’extrême droite, qui a lu Gramsci, mène depuis la fin de la Seconde
Guerre mondiale un « combat culturel » dans lequel la bataille des mots
est fondamentale. Cette bataille vise à fabriquer, dans nos imaginaires, une
réalité alternative extrêmement perverse, dans laquelle George Orwell –
non seulement invoqué, mais approprié – contredit George Orwell lui-même
: dans ce monde, les pires maux ont été fomentés par la furie vengeresse
des femmes ; l’antiracisme est le véritable racisme ; l’antisémitisme
est une invention musulmane ; la démocratie est un totalitarisme et le
quatrième pouvoir un geôlier. Dans ce monde, l’extrême droite dominante
est harassée et censurée par la violence des Droits de l’homme. Dans ce
monde, la haine revêt le masque de la lucidité et les passions tristes celui
de la rationalité.

Nourri par un réseau foisonnant de médias traditionnels et surtout de
nouveaux médias issus d’internet, cet imaginaire a trouvé dans la violence
et le traumatisme des attentats djihadistes un terreau fragilisé par la peine
et la terreur dans lequel a pu s’épanouir la fleur la plus vénéneuse :
celle qui contamine l’espace républicain.

Ce glossaire propose de reconnaître, dans l’écosytème d’un fascisme
qui vient, les mots et les concepts qui ont empoisonné le débat public pour
imposer leurs vues hégémoniques.
