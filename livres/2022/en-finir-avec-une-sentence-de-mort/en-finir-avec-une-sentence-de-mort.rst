.. index::
   pair: Livre; On ne peut pas accueillir toute la misère du monde. En finir avec une sentence de mort (2022)
   pair: Pierre Tevanian ; On ne peut pas accueillir toute la misère du monde. En finir avec une sentence de mort (2022)

.. _sentence_de_mort_2022:

========================================================================================================================================================
2022 **On ne peut pas accueillir toute la misère du monde. En finir avec une sentence de mort** par Pierre Tevanian et Jean-Charles Stevens 
========================================================================================================================================================

- https://anamosa.fr/livre/on-ne-peut-pas-accueillir-toute-la-misere-du-monde/


.. figure:: images/recto.webp

Auteurs: Pierre Tevanian et Jean-Charles Stevens
=====================================================

Pierre Tevanian 
-----------------

- https://anamosa.fr/auteur-autrice/pierre-tevanian/

Pierre Tevanian est philosophe, enseignant, co-animateur du site Les mots sont importants. 

Il est notamment l’auteur de : 

- Dictionnaire de la lepénisation des esprits (L’Esprit frappeur, 2002) ; 
- Le Voile médiatique (Raisons d’Agir, Seuil, 2005) ; 
- La République du mépris (La Découverte, 2007) ; 
- Les filles voilées parlent (collectif, La Fabrique, 2008) ; 
- Dévoilements (Libertalia, 2012) ; 
- La Haine de la religion (La Découverte, 2013) ; 
- Chronique du racisme républicain (Syllepse, 2013) ; 
- La Mécanique raciste (La Découverte, 2017) ; 
- Mulholland Drive. La clef des songes (Dans nos histoires, 2019) ; 
- Politiques de la mémoire (Amsterdam, 2021).


Jean-Charles Stevens
--------------------------

- https://anamosa.fr/auteur-autrice/jean-charles-stevens/

Jean-Charles Stevens est expert juriste et a travaillé pendant une dizaine 
d’années dans plusieurs associations belges de défense du droit des étrangers 
(le CIRÉ, la Ligue des droits humains et l’ADDE). 

Dans ce cadre, il a écrit de nombreux articles sur l’actualité législative 
et jurisprudentielle, principalement relative au droit des étrangers et 
spécifiquement au droit à l’accueil des demandeurs d’asile.


Description
=============

"On ne peut pas accueillir toute la misère du monde" : qui n’a jamais entendu 
cette phrase au statut presque proverbial, énoncée toujours pour justifier 
le repli, la restriction, la fin de non-recevoir et la répression ? 

Dix mots qui tombent comme un couperet, et qui sont devenus l’horizon 
indépassable de tout débat "raisonnable" sur les migrations. 

Comment y répondre ? 

C’est toute la question de cet essai incisif, qui propose une lecture critique, 
mot à mot, de cette sentence, afin de pointer et réfuter les sophismes et les 
contre-vérités qui la sous-tendent. 

Arguments, chiffres et références à l’appui, il s’agit en somme de déconstruire 
et de défaire une "xénophobie autorisée", mais aussi de réaffirmer la 
nécessité de l’hospitalité.

