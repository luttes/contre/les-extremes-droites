.. index::
   pair: Livre ; Extrême droite : La résistible ascension (2024, Collectif)

.. _resistible_ascension_2024:

====================================================================
2024 **Extrême droite : La résistible ascension** par Collectif
====================================================================

- http://www.editionsamsterdam.fr/extreme-droite-la-resistible-ascension/


.. figure:: images/recto.webp

Description
===============

Les progrès électoraux de l’extrême droite ces trente dernières années ont 
installé l’idée de son arrivée inéluctable et imminente au pouvoir. 

Cette idée a trop souvent permis au personnel politique et médiatique de 
s’exonérer de l’analyse des causes profondes et de l’alternative à y opposer.

Cet ouvrage, coordonné par le sociologue Ugo Palheta, propose au contraire 
de comprendre, à l’aide des travaux les plus récents en sciences sociales, 
la façon dont la route a été pavée à l’extrême droite. 

Quelles dynamiques sociales ont poussé une partie croissante des élites et 
certaines fractions des classes populaires à se ranger derrière elle ? 

Par quels médias et sous quelles formes ont été imposés les discours racistes, 
sexistes et LGBTI-phobes qui portent l’extrême-droitisation ? 

Quels réseaux ont appuyé et conforté ce glissement ?

De la préface de l’historien Johann Chapoutot à la postface de Clémence Guetté, 
co-présidente de l’Institut La Boétie, il s’agit ici d’opposer à la fatalité 
une analyse précise des forces, mais aussi des failles, de l’extrême droite. 

Donc de montrer que cette ascension est plus résistible qu’il n’y paraît.

Ugo Palheta
====================

Ugo Palheta est maître de conférences en sociologie à l’Université de Lille, 
rattaché au Centre de recherches sociologiques et politiques de Paris 
(Cresspa) et associé à l’Institut national d’études démographiques (Ined). 

Il est codirecteur de la publication du site Contretemps (**du site et non de la revue, NDLR**) 
et animateur du podcast "Minuit dans le siècle". 

Il a notamment publié La Possibilité du fascisme. France : trajectoire du 
désastre (La Découverte, 2018).

Note sur le site contretemps (NDLR)
-----------------------------------------

- :ref:`antisem:site_contretemps`

Ugo Paleta est le codirecteur non pas de la revue 'Contretemps' mais du site
web 'https://www.contretemps.eu/projet/'.

.. warning:: Le site web est désormais connu pour être mélenchoniste.
   Il a publié un article particulièrement ignoble sur les organisations juives
   de gauche ici: https://www.contretemps.eu/7-octobre-sionisme-gauche-antisemitisme-israel/


.. warning::
   La revue contretemps est dirigée par Francis Sitel
   https://www.syllepse.net/contretemps-_r_94_va_1.html
