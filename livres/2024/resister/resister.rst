.. index::
   pair: Livre ; Résister (2024-10, Salomé Saqué)
   pair: Salomé Saqué ; Résister (2024-10)

.. _resister_2024:

====================================================================
2024-10 **Résister** par Salomé Saqué
====================================================================

- https://www.payot.ch/Detail/resister-salome_saque-9782228937597


.. figure:: images/recto.webp


Auteure : Salomé Saqué
=========================

Salomé Saqué est journaliste pour le média indépendant en ligne Blast. 

Elle est l'autrice de Sois jeune et tais-toi, paru en 2023 aux éditions Payot. 


Description
===============

L'extrême droite est aux portes du pouvoir. 

Dans les urnes comme dans les esprits, ses thèmes, son narratif et son vocabulaire 
s'imposent. 

Il est encore temps d'inverser cette tendance, à condition de comprendre les 
rouages de cette progression et de réagir rapidement. 
