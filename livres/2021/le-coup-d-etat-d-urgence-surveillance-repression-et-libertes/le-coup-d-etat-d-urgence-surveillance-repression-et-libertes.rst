

.. _coup_d_etat_d_urgence_2021_01_21:

===================================================================================================
2021-01-21 **Le Coup D'État D'Urgence, Surveillance, Répression Et Libertés** par Arié Alimi
===================================================================================================

- https://www.librairiesindependantes.com/product/9782021469219/
- :ref:`antisem:arie_alimi`


.. figure:: images/couverture.webp


Résumé
=========

Printemps 2020. Pour faire face au Covid-19, le premier état d’urgence
sanitaire de l’histoire de France est instauré, s’inspirant de l’état
d’urgence décrété pendant la guerre d’Algérie. 

Du jour au lendemain,
l’intégralité de la population française se retrouve assignée à
résidence, privée de sa liberté d’aller et de venir, de son droit
à la vie privée et, selon les cas, de son droit au travail ou à la
liberté d’entreprendre. 

Parallèlement, un mécanisme de surveillance
généralisée est mis en place, avec quadrillage policier du territoire
et usage de drones. Désormais, chaque citoyen est considéré comme un
danger potentiel. 

Il n’est plus un sujet de droit mais un "sujet virus". 

Alors que l’état d’exception contamine peu à peu le droit commun à
la manière d’une tache d’huile, les catégories de personnes et les champs
touchés par les réductions de libertés ne cessent de s’étendre. 

Quelles conséquences, dans ces conditions, pour les libertés publiques ? 
Quels contre-pouvoirs mobiliser face à l’arbitraire de l’exécutif ? 
Faut-il apprendre à vivre avec ce nouveau paradigme, ou, position défendue par
l'auteur, ne pas s'y résigner ? 

L’expertise d’Arié Alimi est précieuse
et permet de poser un regard sans concession sur la question des libertés
publiques et des dérives policières, au cœur de l’actualité. 

Face à ce "coup d’état d’urgence", il est encore temps de réagir. 

Arié Alimi est un avocat pénaliste. Il défend de nombreuses victimes de violences
policières et est membre de la Ligue des droits de l’Homme.

Caractéristiques
==================

EAN13
    9782021469219
Éditeur
    Seuil
Date de publication
    21 janvier 2021
Collection
    Documents (H. C.)
Nombre de pages
    175
Dimensions
    18,5 x 13 x 1,5 cm
Poids
    185 g
Langue
    fre 
