.. index::
   ! Glossaire

.. un·e

.. _glossaire:

===================================================================================
Glossaire
===================================================================================

.. glossary::

   UltraDroite
       Source :ref:`turchi_2023_12_01`

       Mediapart et l’usage du terme "ultradroite"
       
       Mediapart a commencé à employer le terme d’"ultradroite" en 2013, à 
       l’occasion de la mort du militant antifasciste Clément Méric, tué par 
       des skinheads, et lors des violences émaillant les manifestations 
       contre le mariage pour tous (ici et là).

       Au fil des années, nous avons eu à la rédaction de nombreux débats sur 
       l’emploi de ce vocable, essentiellement policier, employé comme fourre-tout 
       par les médias.

       Aujourd’hui, cette manière de nommer les choses ne nous paraît plus 
       correspondre à la réalité et sa complexité. 
       La mouvance d’extrême droite est faite d’une multitude de chapelles, 
       en constante recomposition, avec une porosité et des alliances ponctuelles 
       importantes, comme nous l’avons raconté. 
       Impossible, dès lors, de tracer une ligne étanche entre extrême droite 
       partisane et ultradroite violente.

       Considérant que l’époque nécessite le maximum de clarté, **Mediapart 
       parlera donc désormais d’"extrêmes droites"**, en distinguant les modes 
       d’action – politique, violent ou terroriste.
