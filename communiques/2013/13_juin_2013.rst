
.. index::
   pair: Antifa; 13 juin 2013

.. _tract_antifa_unitaire_13_juin_2013:

========================================================================================
Le fascisme tue. Ensemble, combattons-le ! 
========================================================================================


Le 5 juin, des militants d'extrême-droite ont tué Clément Méric, 
syndicaliste  étudiant et militant antifasciste. 

Ce meurtre nous indigne et nous révolte ; il s'inscrit dans la suite de 
très nombreuses agressions commises par des groupes d'extrême-droite 
ces derniers mois. 
La situation exige des actes forts, permettant de mettre un coup d'arrêt 
à la propagation de ces idées et pratiques nauséabondes.

Dans le respect de leurs différences, les organisations soussignées 
appellent à s'unir pour rendre hommage à Clément et pour éliminer la 
haine fasciste.

Confortés par des partis qui reprennent des propos et des pratiques de 
l'extrême droite, les groupes fascistes refont surface. 
Les dernières actions contre le mariage pour tous et toutes ont été 
l'occasion pour eux d'être mis sur le devant de la scène. Nous dénonçons 
la banalisation du FN et de ses idées xénophobes et racistes.

L'exclusion, le rejet de l'autre, la fermeture des frontières, la 
désignation de boucs émissaires, la dénonciation de l'immigration comme 
responsable de tous les maux sont des attitudes qui, l'histoire en 
témoigne, conduisent au pire. 

L'Etat entretient un climat délétère en organisant des expulsions massives 
qui participent à la stigmatisation des immigré-es et des Roms. 

Au contraire, il est nécessaire d'agir avec détermination contre les 
commandos fascistes.
Odieux et inacceptable en lui-même, le meurtre de Clément dépasse le drame 
individuel. 
Agressions contre les lesbiennes, bi-es, gays et les personnes trans, 
contre les immigré-es et les personnes issu-es de l'immigration, les 
musulman-es, actes antisémites, violences envers des militant-es 
antifascistes et des organisations progressistes, se sont multipliées 
dans toute la France comme à travers toute l'Europe. 

Le mensonge, la haine, la violence, la mort, voilà ce que porte 
l'extrême-droite, de tout temps et en tous lieux.

Ce n'est pas une question morale ; le fascisme se nourrit des peurs face 
à l'avenir : 5 millions de chômeurs et chômeuses, 8 millions de personnes 
vivant sous le seuil de pauvreté, 3,5 millions de mal logé-es, accroissement 
de la précarité, conditions de travail dégradées, licenciements, fermetures 
d'entreprises...  

Face à l'explosion des inégalités et aux politiques d'austérité, il faut 
reconstruire l'espoir collectif en une société plus juste. 
**La question de la répartition des richesses que nous produisons est 
fondamentale**. 

L'extrême-droite est à l'opposé de ces valeurs.

Utiliser la mort de Clément serait méprisable. 

A contrario, c'est honorer sa mémoire que de dire publiquement et ensemble 
ses engagements syndicaux et antifascistes, et de poursuivre encore plus 
nombreux-euses et déterminés-es ses combats pour la liberté et une autre 
société.

Unité contre le fascisme et l'extrême-droite !

Manifestation à Paris, dimanche 23 juin à 15h

Des manifestations seront aussi organisées en commun dans d'autres 
villes.

Nos organisations se réuniront de nouveau après la manifestation : 
éradiquer la menace fasciste nécessite un travail dans la durée et 
l'organisation de collectifs locaux. 

Nos organisations sont différentes, mais elles ont un point commun 
essentiel : le refus de l'intolérance, du nationalisme, de la haine, et 
de l'exclusion ; tout le contraire de ce que veut imposer l'extrême-droite !

Le fascisme et l'extrême-droite ne sont pas des courants politiques avec 
lesquels on dialogue ou on compose. 
Leur système est basé sur la violence physique, la haine, l'asservissement 
des peuples.

Premiers signataires (au 13 juin) :

- AC !, 
- Act Up Paris, 
- Action antifasciste Paris Banlieue, 
- Alternative Libertaire, 
- APEIS, ATTAC France, 
- CADAC, CEDETIM/IPAM, 
- CGT Educ'action Versailles, 
- CGT Educ'action Créteil, 
- CNDF, 
- CNT, 
- Collectif Antifasciste Paris Banlieue, 
- Collectif CIVG Tenon, 
- CONEX (Coordination nationale contre l'extrême droite), 
- Collectif de Saint Denis contre le FN et l'extrême droite, 
- Confédération paysanne, 
- Convergence et Alternative, 
- DAL, 
- DIDF, 
- EELV, 
- FA, 
- FASE, 
- FASTI , 
- Fédération Anarchiste, 
- FIDL, 
- FSU, 
- Fondation Copernic, 
- Gauche Anticapitaliste, 
- Gauche Unitaire, 
- GISTI, 
- Jeudi Noir, 
- Justice et Libertés, 
- L'appel et la pioche, 
- La Horde, 
- La LMDE, 
- Les Alternatifs, 
- Lesbian and Gay Pride Lyon, 
- Les Debunkers, 
- Marche Mondiale des femmes, 
- Marches européennes contre le chômage, 
- MRAP, 
- Mémorial 98, 
- MJCF, 
- MNCP, 
- M'PEP, 
- NPA, 
- PCF, 
- PCOF, 
- PG, 
- Pink Bloc Paris, 
- Ras l'Front Marne-la-Vallée, 
- Ras l'Front 38, 
- République et Socialisme, 
- Réseau pour un avenir sans fascisme, 
- SGEN-CFDT Académie de Versailles, 
- SLU (Sauvons l'université), 
- SNESUP-FSU, 
- Solidaires Etudiant-Es, 
- SOS Racisme, 
- Sortir du colonialisme, 
- Syndicat des avocats de France, 
- Syndicat de la magistrature, 
- UNEF, 
- Union syndicale Solidaires, 
- UNSP, 
- VISA (Vigilance et initiatives syndicales antifascistes)



