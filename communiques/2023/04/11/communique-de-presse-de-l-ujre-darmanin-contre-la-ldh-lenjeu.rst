.. index::
   pair: Communiqué de presse; UJRE (2023-04-11)

.. _ujre_2023_04_11:

==============================================================================
2023-04-11 **Communiqué de presse de-l'UJRE Darmanin contre la LDH l'enjeu**
==============================================================================



:download:`Télécharger le communiqué au format PDF <pdfs/COMMUNIQUE_UJRE_DARMANIN_CONTRE_LA_LDH_LENJEU_2023_04_11.pdf>`

::

    Union des Juifs pour la Résistance et l’Entraide
    14 rue de Paradis 75010 PARIS
    Tel : 01 47 70 62 16 Mèl : secretariat@ujre.fr Site : https://ujre.fr
    Président d’honneur : Adam Rayski
    Coprésidents : Jacques Lewkowicz, Claude Sarcey, Raymonde Baron
    Présidente déléguée : Claudie Bassi-Lederman

Darmanin contre la LDH : l’enjeu
===================================

Mercredi 5 avril 2023, le ministre de l’Intérieur, s’adressant successivement
aux deux chambres parlementaires, n’a pas hésité, contre toute vérité, à
affirmer l’absence d’incidents majeurs depuis trois ans dans son domaine
de compétence.

Il a mis en avant une légitimité de l’usage de la contrainte donnée par
le droit aux forces de l’ordre.

Mais il s’est refusé à assumer que ceci les contraignait à d’autant plus
de devoirs.

Lorsque des députés en rappelaient la nécessité, il est allé jusqu’à parler
de « terrorisme intellectuel », alors même que des réactions internationales
s’émeuvent d’un « usage excessif de la force en France ».

De plus, Gérald Darmanin s’en est pris à « une ultra gauche » qui aurait
« infiltré le mouvement social » alors qu’une interrogation critique
amène à présumer que cet extrémisme pourrait, parmi d’autres hypothèses,
être un produit de l’activité policière destiné à déconsidérer le mouvement social.

C’est dans ce contexte que le ministre a, contre toute évidence, qualifié
de « fake news » les enregistrements de l’action des gendarmes à Sainte-Soline
réalisés par la Ligue des droits de l’homme, qui démontrent le blocage,
par les gendarmes, de la circulation du SAMU venant au secours des blessés.

Le ministre est même allé jusqu’à mettre en cause la subvention reçue par la LDH.

L’Union des Juifs pour la Résistance et l’Entraide (UJRE) condamne fermement
cette attaque contre une organisation amie car elle rappelle que la Ligue
des droits de l’homme est une association de défense des droits humains
se donnant pour objectif d'observer, défendre et promouvoir les droits de
l’homme au sein de la République française, dans toutes les sphères de
la vie publique.

Elle est née dans le sillage de l’Affaire Dreyfus et agit pour le respect
des droits fondamentaux rassemblés dans les déclarations des droits de
l’homme de 1789, 1793 et dans la déclaration universelle de 1948.

L’UJRE considère qu’à travers cette attaque contre la LDH, il ne s’agit
pas d’une simple dérive due au manque de lucidité du ministre, mais plutôt
de l’un des aspects d’une stratégie consciente et conduite avec fermeté
par le pouvoir en place, qui consiste à organiser la concurrence entre
elles de toutes les forces de travail, pour en minimiser le coût, tout
en épuisant les ressources naturelles dont on abandonne toute protection,
en vue de poursuivre l’accumulation du capital financier.

Tous les obstacles sur le chemin de cet objectif, notamment les organisations
syndicales, celles de défense des droits humains et de la nature doivent
être éliminés et, si besoin, réprimés violemment.

Pour participer au combat contre une telle stratégie et contribuer à la
lutte pour la priorité donnée à l’humain, l’UJRE, fidèle à ses engagements
tenus depuis 80 ans, se tiendra aux côtés de la Ligue des droits de
l’homme pour lui permettre de poursuivre ses activités quelles qu’en soient
les conditions.

::

    UJRE - 11 avril 2023
    L’Union des Juifs pour la Résistance et l’Entraide (UJRE)
    est une association progressiste juive et laïque née en 1943 dans la
    Résistance à l’occupant nazi
