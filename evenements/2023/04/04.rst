
.. _evenements_2023_04:

=================================================================
2023-04
=================================================================

#Terrorisme #EtatFrançais #Fascisme


Crack-Up Capitalism by Quinn Slobodian review – the economic anarchy of Liz Truss’s dreams
===============================================================================================

- https://www.theguardian.com/books/2023/apr/25/crack-up-capitalism-by-quinn-slobodian-review-market-radicals-and-dream-of-world-without-demoracy-the-economic-anarchy-of-liz-truss-dreams


2023-04-11  **Les attaques dont la LDH fait l'objet n'entameront en rien notre détermination**
===============================================================================================

- https://www.change.org/p/stop-%C3%A0-l-escalade-r%C3%A9pressive/u/31485063?cs_tk=As1jHboocpL-P9rKPmQAAXicyyvNyQEABF8BvLyb2icVoJWJ2a4ulzBEMvM%3D


Chers tous, chères toutes,

Encore une fois, un grand merci pour votre soutien.

L'actualité nous amène sans cesse à renforcer notre volonté de lutter
pour la défense de l'Etat de droit. Dans ce cadre, nous avons fait de la
lutte contre les violences policières une de nos priorités.

Or, la LDH a fait l'objet de menaces à peine voilées du ministre de
l'Intérieur.

En effet, lors de son audition au Sénat sur la question de la manifestation
contre la « mégabassine » de Sainte-Soline, Gérald Darmanin a en effet
semblé remettre en cause les financements publics qui nous sont accordés,
“eu égard aux actions que nous avons pu mener”.

Cette manace est particulièrement grave.

Depuis plus de 120 ans, la LDH défend les droits et les libertés pour
toutes et tous face à un pouvoir qui a toujours la tentation d'en abuser,
même en démocratie.

C'est notre seule boussole et cela continuera à l'être, quoi qu'en pense
Gérald Darmanin, quoi qu'il tente pour entraver notre expression.

Les attaques dont la LDH fait aujourd’hui l'objet n'entameront en rien
notre détermination.

Plus que jamais, nous avons besoin de vous pour amplifier notre voix.

Plus que jamais, nous continuerons à dénoncer les atteintes aux droits
et libertés.

Alors signez et partagez autour de vous, tant que vous le pouvez, cette pétition.

N'hésitez pas à nous soutenir : https://soutenir.ldh-france.org/

Patrick Baudouin, président de la LDH
et Marie-Christine Vergiat, vice-présidente de la LDH



2023-04-07 Les propos de Darmanin sur la LDH créent un début de malaise dans la majorité présidentielle
=============================================================================================================

- https://www.mediapart.fr/journal/politique/070423/les-propos-de-darmanin-sur-la-ldh-creent-un-debut-de-malaise-dans-la-majorite-presidentielle

Les propos de Darmanin sur la LDH créent un début de malaise dans la
majorité présidentielle

Si le silence continue de rester la règle au sein de la Macronie, quelques voix,
timides, s’élèvent, pour prendre leurs distances avec les propos du
ministre de l’intérieur qui a mis en cause le financement de l’association
de défense des droits de l’homme.

**Pas de hauts cris d’indignation ou de condamnation formelle.**

Ni l’Élysée ni Matignon n’y ont d’ailleurs trouvé à redire. Depuis que
Gérald Darmanin a caressé, mercredi, l’hypothèse de « dé-financer » la
Ligue des droits de l’homme, la Macronie reste, en apparence, de marbre.

C’est peu dire pourtant que le ministre de l’intérieur a créé la stupeur.

Au sein du secteur associatif mais aussi de la gauche partisane qui a
aussitôt apporté son soutien à l’organisation née au moment de l’affaire
Dreyfus et rappelé que le dernier à l’avoir prise pour cible n’était
autre qu’un certain… Philippe Pétain.


Des propos « d’une gravité extrême », a dénoncé l’actuel président de la
LDH, Patrick Baudouin, quand son prédécesseur, l’avocat Henri Leclerc,
y a vu « la preuve que Gérald Darmanin ne supporte pas que l’on vérifie
la conformité au droit du comportement de la police »

...

La preuve d’un consentement tacite - et massif - aux propos du ministre
de l’intérieur ? Le député du Val-de-Marne Mathieu Lefèvre veut le
croire : « Il n’y a pas eu de débat entre nous [les députés du groupe
Renaissance - ndlr] depuis mercredi, mais je pense que tout le monde est
sur cette ligne », indique-t-il, peinant à voir où ces propos pourraient
poser problème puisque « Gérald Darmanin a simplement rappelé qu’il n’y
a pas de droit à vie de recevoir des subventions ».

...


L’apathie du camp présidentiel illustre aussi l’impunité dont jouit le
ministre de l’intérieur en interne. Depuis son arrivée Place Beauvau,
en 2020, il a fait des coups de menton sa marque de fabrique et poussé,
toujours plus loin, les curseurs politiques du macronisme.

Loi « Séparatisme », loi « sécurité globale », dissolutions d’associations,
propos sur les rayons « halal » dans les supermarchés ou sur les violences
policières… Gérald Darmanin ose tout, dans une fièvre très sarkozyste,
sans jamais être rappelé à l’ordre.

Sur la route de son ambition personnelle, l’ancien maire de Tourcoing (Nord),
qui en réfère directement au président de la République sans jamais passer
par Matignon, n’accepte que peu d’embûches. À rebours des consignes, le
ministre de l’intérieur se saisit des sujets qu’il veut et s’invite dans
des médias choisis, au gré de son agenda politique.

Un agenda qui vise, sans nul doute, déjà l’après-Macron et qui implique,
selon Éric Coquerel, de multiplier les « provocations ».

« Il considère que l’avenir du macronisme se jouera à droite et à l’extrême droite,
observe l’Insoumis, et tente, comme Sarkozy, d’assécher l’électorat du
Rassemblement national. »



.. _darmanin_2023_04_07:

2023-04-07 Sur la LDH, Gérald Darmanin dans l’extrême droite ligne du Rassemblement national
=================================================================================================

- https://www.mediapart.fr/journal/politique/070423/sur-la-ldh-gerald-darmanin-dans-l-extreme-droite-ligne-du-rassemblement-national


En fustigeant mercredi 5 avril 2023 la Ligue des droits de l’homme, le ministre de
l’intérieur n’en était pas à son coup d’essai. En 2015, alors maire de
Tourcoing, il avait déjà voulu annuler la subvention de la ville versée
à la LDH. S'inscrivant ainsi dans une tradition d’extrême droite : s’en
prendre aux défenseurs des libertés fondamentales.


OnOn a les soutiens qu’on mérite. L’anathème jeté par le ministre de l’intérieur,
Gérald Darmanin, contre la Ligue des droits de l’homme (LDH), mercredi
lors d’une audition devant les parlementaires, a fait bondir les défenseurs
des droits humains – et permis à la LDH de recevoir 30 000 euros de dons
en 24 heures.

Mais il a surtout fait plaisir à ses ennemis, à l’exemple de l’essayiste
obsédée par l’islam Céline Pina.

...

Un condensé des paniques morales de l’époque pour une droite qui flirte
toujours plus avec l’extrême droite. Car Gérald Darmanin n’en est pas à
son coup d’essai. Alors maire de Tourcoing (Nord) (et encore à l’UMP),
en 2015, celui-ci avait voulu annuler la subvention de la ville versée
à la LDH, au motif que cette dernière envisageait de subventionner à son
tour un collectif de soutien aux Roms. La subvention – 250 euros pour
l’année ! – avait fini par être concédée, « malgré le contexte budgétaire
particulièrement difficile de la ville », selon le courrier de l’édile.

...

Cet alignement des planètes entre la justice et le politique confine au
fascisme d’atmosphère. Et explique peut-être pourquoi, deux jours avant
sa sortie contre la LDH, le ministre de l’intérieur Gérald Darmanin avait
préféré se faire remplacer par son ministre délégué chargé des outre-mer,
Jean-François Carenco, pour répondre aux député·es lors d’un débat sur
la lutte contre le terrorisme d’extrême droite.

Quant à Emmanuel Macron, depuis la Chine, il a sans le vouloir souligné
ce qui le séparait du Rassemblement national en déclarant : « Si les gens
voulaient la retraite à 60 ans, ce n’était pas moi qu’il fallait élire
comme président de la République. »

Comme s’il n’y avait plus que l’âge de départ à la retraite qui opposait
la droite et l’extrême droite.

2023-04-04 Philippe Descola : « Darmanin fait de la vieille politique au service du vieux monde »
====================================================================================================

- https://reporterre.net/Philippe-Descola-Darmanin-fait-de-la-vieille-politique-au-service-du-vieux-monde


Le « terrorisme intellectuel » employé par Gérald Darmanin sert à « faire diversion »,
selon l’anthropologue Philippe Descola, et à rendre illégitimes
Les Soulèvements de la Terre.

Mais « on ne peut pas dissoudre des idées ».

Philippe Descola est membre du Collège de France et titulaire de la chaire
d’Anthropologie de la nature.
Il est aussi coprésident de l’Association pour la défense des terres,
appui financier des Soulèvements de la Terre.
