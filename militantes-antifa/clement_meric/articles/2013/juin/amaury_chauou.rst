
.. index::
   pair: Amaury ; Chauou
   pair: Amaury Chauou; Clément Méric


.. _amaury_chauou:

==========================================================================
Par Amaury Chauou, professeur d'histoire en classes préparatoires
==========================================================================


.. seealso::

   - http://www.liberation.fr/societe/2013/06/10/clement-meric-un-citoyen-du-monde-sachant-dire-non_909530


Par AMAURY CHAUOU Professeur d'histoire en classes préparatoires
(Lycée Kerichen, Brest)

Le ventre est encore fécond, d’où a surgi la bête immonde, et Clément
est tombé. Combien de Clément faudra-t-il pour que l’on nous débarrasse
sur la place publique de ces vautours de la démocratie qui prospèrent de
ses bas morceaux ?

Brillant élève assidu à mon cours d’option Sciences Po à Brest en 2012,
Clément, loin d’être un gros bras de l’extrême gauche militante, était
avant tout un jeune intellectuel et un citoyen du monde sachant dire non.
Non d’abord à la maladie, le cancer qu’il avait combattu durant sa classe
de première avec une volonté de fer, forçant l’admiration.

Non ensuite au conformisme de la pensée, la synthèse « molle ».
J’ai souvenir d’avoir corrigé une de ses copies, consacrée à l’Allemagne
durant les années 1930 où transparaissaient une culture et une maturité
politiques étonnantes pour son jeune âge.

Admis à plusieurs Instituts d’Études politiques différents, le genre
d’étudiant que tout jury de concours recherche, Clément était
naturellement parti à Paris pour parfaire sa formation rue Saint-Guillaume
et faire vivre ses idées.

Qu’a-t-il trouvé sur les bords de Seine ? De jeunes étudiants engagés
comme lui dans des combats intransigeants : contre l’homophobie,
contre le racisme, contre l’antisémitisme, contre l’ultralibéralisme,
contre les inégalités hommes/femmes.

Il n’y a rien là du nihilisme  que ses liens
passés avec la CNT pourraient suggérer.


Mais à Paris, à ses dépens, Clément a aussi expérimenté un climat de
tensions depuis plusieurs mois, palpable lors de débordements publics.
La culture de l’occupation de la place publique l’animait certes, mais
pour le tractage et les rites d’unité, bien loin de toute accoutumance
au coup de poing auquel, du reste, sa menue silhouette et sa récente
convalescence ne le prédisposaient pas.

Là était sans doute sa faiblesse : Clément, sous réserve des conclusions
de l’enquête criminelle à venir, a croisé des spécialistes des violences
urbaines, abonnés aux discours de rupture et de haine politique et sociale
que profèrent certains éléments de la jeunesse française d’aujourd’hui.



La mauvaise rencontre lui a été fatale, mais sa voix demeure : s’il se
revendiquait du mouvement social et s’il combattait les idées des
groupuscules d’extrême droite, la personnalité de ceux qui crachent à la
figure de la République et rêvent de troisième voie lui était indifférente.
Le jeune homme savait pertinemment que le militantisme politique vise
des adversaires pour ce qu’ils font, non pour ce qu’ils sont.
En ce sens, le registre d’expression de Clément, d’un tempérament discret
et un peu réservé, était aux antipodes de la culture de l’ultra-violence
des skinheads.



À 18 ans, Clément avait toute la vie devant lui. Il n’est pas tolérable
qu’en France en 2013 on puisse tomber sous les coups de la haine et du
fanatisme. L’indignation et l’émotion suscitées par l’exacerbation de la
violence doivent réunir tous ceux qui, comme Clément, veulent vivre leur
liberté passionnément. Il est grand temps de dire non à ce que nous ne
voulons plus voir dans notre démocratie empoisonnée.

