

.. _clement_meric_5_juin_2013:

=====================================================
Assassinat de Clément Méric le mercredi 5 juin 2013
=====================================================

.. contents::
   :depth: 3   

Articles 
========

comite-pour-une-nouvelle-resistance
------------------------------------

.. seealso::

   - http://comite-pour-une-nouvelle-resistance.over-blog.com/article-no-pasaran-a-clement-meric-118312168.html
   

::

    No pasaran ?
    =============

    Trop tard, ils sont passés !
    Ils sont là en pleine rue, leurs faces hideuses
    de haine et de sang
    ont tombé les masques du passé.

    No pasaran ?
    =============

    Trop tard ils ont frappé !
    Clément le premier d’une longue série,
    paie de son sang le renoncement.
    La bête immonde est dans nos murs,
    elle rôde à l’affût du sang frais,
    elle se délecte d’un avenir de pus,
    de fientes coiffant son crâne rasé,
    elle se délecte de la peur sur son chemin,
    des frissons dressant les poils de la main.

    No pasaran ?
    =============

    Mais qu’avez-vous fait ?

    Droite et gauche, la tolérance,
    l’ignorance des faits,
    la cohabitation,
    vous ont rendu coupables,
    vous avez sur vos doigts et vos âmes
    le rouge-sang de nos enfants qui paieront
    vos coupables défections.

    Clément petite étoile allumée dans le ciel,
    puisses-tu de ton martyr éveiller les consciences,
    que cette bête horrible qui de sa présence
    souille nos jours et nos nuits,
    notre passé et notre avenir
    de toute sa pestilence,
    puisse à nouveau se terrer dans son trou
    d’immondices,
    qu’elle y retourne à jamais.
    Car telle est sa place.

    J’allume ton étoile avec l’allumette de l’espoir,
    celle qui fait briller nos luttes
    qui dit aux camarades qu’il faut force et courage
    car le combat sera rude,
    les victimes tomberont.

    Le sang dans les rues de nos enfants perdus
    commence juste à couler,
    et l’histoire si cruelle dans sa répétition
    nous fait signe qu’un jour,
    il faudra arrêter.
    Arrêter d’hocher la tête,
    de politiser de l’autruche et du croupion,
    d’accorder sans cesse nos violons
    pour X raisons.

    No pasaran ?
    ==============

    Trop tard, ils sont passés .

     
    Carole Radureau (06/06/2013)
    http://cocomagnanville.over-blog.com/no-pasaran-a-cl%C3%A9ment-m%C3%A9ric


Communiqué : Solidaires Sciences Po
===================================

.. seealso::

   - http://comite-pour-une-nouvelle-resistance.over-blog.com/article-l-agression-criminelle-contre-clement-meric-118311078.html
   - http://sudsciencespo.wordpress.com/2013/06/06/pour-clement/
 
 
Communiqué : Solidaires Sciences Po

Clément Méric, 18 ans, un jeune camarade de Solidaires étudiant-e-s a
été battu à mort hier après-midi par des militants fascistes vers 
St Lazare. Ci-dessous un communiqué de Solidaires étudiant-e-s sciences-po :
 
http://sudsciencespo.wordpress.com/2013/06/06/pour-clement/
 
Le mercredi 5 juin 2013, en sortant d’un magasin de vêtements, près de 
la gare Saint-Lazare, notre camarade Clément, syndicaliste à Solidaires 
Etudiant-e-s et militant antifasciste a été battu à mort par des membres 
de l’extrême droite radicale. 

Clément est décédé des suites de ses blessures,  dans la nuit, à l’hôpital 
de la Pitié-Salpêtrière.

La mort de notre camarade s’inscrit dans le contexte de la progression 
d’un mouvement fasciste violent en France et ailleurs en Europe. 
Sa  perte nous accable et notre douleur est encore aggravée par la 
certitude que nombreux et nombreuses sont celles et ceux qui, militant-e-s 
antifacistes, personnes exposées à l’homophobie et/ou au racisme, auraient 
pu et peuvent encore en être victimes.

Aujourd’hui, toutes nos pensées vont à sa famille et à ses proches à qui 
nous exprimons toute notre solidarité.

A la mémoire de notre camarade et ami, rendez vous à 17h aujourd’hui 
jeudi 6 juin au passage du havre, Métro Havre Caumartin.

Solidaires Étudiant-e-s SciencesPo 
   
