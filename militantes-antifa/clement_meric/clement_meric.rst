.. index::
  pair: Clément; Méric
  ! Clément Méric

.. _clement_meric:

=====================================================
Clément Méric (1995-5 juin 2013)
=====================================================

.. seealso::

   - http://fr.wikipedia.org/wiki/Cl%C3%A9ment_M%C3%A9ric


.. contents::
   :depth: 3

Clément Méric
==============


.. figure:: clement_meric.jpg
   :align: center


==========
Biographie
==========

Biographie

Clément Méric est le fils de deux enseignants de droit de l'université
de Bretagne occidentale à Brest.

Il est étudiant au lycée de l'Harteloire de cette même ville, où il obtient
un baccalauréat scientifique avec mention « bien », c'était un « très bon élève »
et « un gamin un petit peu rebelle » mais « tout à fait respectueux des
règles », d'après son ancien proviseur Jean-Jacques Hillion du lycée de
l'Harteloire.

Il commence à militer dès ses 15 ans à la Confédération nationale du travail
dans ce cadre, il anime localement un mouvement contre la réforme du lycée
en 2010.

Il va ensuite étudier à l'Institut d'études politiques de Paris, et
commence à militer syndicalement au sein de Solidaires Étudiant
(anciennement SUD Étudiant) et à fréquenter le kop Bauer des supporters
du Red Star.

Il participe aussi à des contre-manifestations en marge de manifestations
contre le mariage homosexuel début-2013.
Il était « réservé, il ne cherchait pas la violence », selon un de ses
enseignants.



Articles
=========


.. toctree::
   :maxdepth: 3

   articles/articles

